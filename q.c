/*
** $Id: q.c,v 4a9de31628cd 2007/04/20 21:53:14 mikek $
**
** Queue management functions adapted from the XINU Operating System.
**
** 
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "q.h"

#define MININT		0x80000000L
#define MAXINT		0x7fffffffL

#define QHEAD(qp)	((qp)->size)
#define QTAIL(qp)	((qp)->size + 1)

/**
 * newqueue - create a new queue data structure.
 * @size: maximum number of items.
 *
 * Construct a new queue object.  The queue uses management functions adapted 
 * from the XINU Operating System, for reference see "Operating System Design,
 * Volume I" by Comer and Fossum.  The queues can be used to implement FIFOs, 
 * sorted lists, and delta lists.  A queue is implemented as an array of 
 * qent_t elements.  The last two elements of the array serve as the head 
 * and tail of the queue.
 *
 * The sorted list (priority queue) and delta list implementations store the
 * entries in non-descending order.  Insertions are O(N) due to a linear search
 * of the queue but removals are O(1).
 *
 * Returns queue pointer or NULL if there is insufficient memory.
 */
Q_t*
newqueue(int size)
{
    Q_t		*qp;
    
    /*
     * We need size+2 qent_t elements (the extras are for the HEAD and
     * TAIL elements).  There is already space for 1 in the Q_t size.
     */
    if((qp = (Q_t*)malloc(sizeof(Q_t) + (size+1)*sizeof(qent_t))) == NULL)
	return NULL;

    qp->size = size;
    clear_queue(qp);
    
    return qp;
}

/**
 * clear_queue - remove all queue entries.
 * @qp: queue pointer.
 *
 * Clear all of the entries from a queue.
 */
void
clear_queue(Q_t *qp)
{
    qent_t	*hptr, *tptr;
    int		hindex, tindex;

    qp->nr_entries = 0;
    qp->is_delta = 0;
    
    hindex = QHEAD(qp);
    tindex = QTAIL(qp);
    
    hptr = &qp->q[hindex];
    tptr = &qp->q[tindex];
    hptr->qnext = tindex;
    hptr->qprev = EMPTY;
    hptr->qkey = MININT;
    tptr->qnext = EMPTY;
    tptr->qprev = hindex;
    tptr->qkey = MAXINT;

}

/**
 * delete_queue - destroy a queue.
 * @qp: queue pointer.
 *
 * Free all memory associated with a queue.
 */
void
delete_queue(Q_t *qp)
{
    free(qp);
}

/**
 * dump_queue - output an ascii representation of a queue
 * @qp: queue pointer
 * @ofp: pointer to output file
 *
 * Dump the contents of a queue to a file.
 */
void
dump_queue(Q_t *qp, FILE *ofp)
{
    int		next, i;
    
    fprintf(ofp, "%d -> ", QHEAD(qp));
    next = qp->q[QHEAD(qp)].qnext;
    i = 0;
    
    while(next != EMPTY && i++ < qp->size)
    {
	fprintf(ofp, "%d(%08lx) -> ", next, qp->q[next].qkey);
	if(next == qp->q[next].qnext)
	{
	    fprintf(ofp, "*** LOOP ***");
	    break;
	}
	
	next = qp->q[next].qnext;
    }
    fprintf(ofp, "END\n");
}

/**
 * enqueue - add an item to the head of the queue.
 * @qp: queue pointer
 * @item: item index (0 <= @item < queue size)
 *
 * Add an item to the head of a queue.  Returns 1 if successful, otherwise 0.
 */
int
enqueue(Q_t *qp, int item)
{
    qent_t	*tptr, *mptr;

    if(item >= qp->size)
	return 0;

    qp->nr_entries++;
    
    tptr = &qp->q[QTAIL(qp)];
    mptr = &qp->q[item];
    mptr->qnext = QTAIL(qp);
    mptr->qprev = tptr->qprev;
    qp->q[tptr->qprev].qnext = item;
    tptr->qprev = item;
        
    return 1;
}

/**
 * dequeue - remove an item from the queue.
 * @qp: queue pointer
 * @item: item index (0 <= @item < queue size)
 *
 * Returns @item
 */
int
dequeue(Q_t *qp, int item)
{
    qent_t	*mptr = &qp->q[item];

    if(item >= qp->size)
	return -1;
    
    qp->nr_entries--;
    qp->q[mptr->qprev].qnext = mptr->qnext;
    qp->q[mptr->qnext].qprev = mptr->qprev;
    
    if(qp->is_delta && qp->nr_entries && mptr->qnext != QTAIL(qp))
	qp->q[mptr->qnext].qkey += mptr->qkey;
    
    return item;
}

/**
 * insert - add item to sorted queue.
 * @qp: queue pointer
 * @item: item index (0 <= @item < queue size)
 * @key: sort key.
 *
 * Insert an item into the queue using @key to determine
 * the order.  Items are stored in ascending key order.
 * Returns 1 if successful, otherwise 0.
 */
int
insert(Q_t *qp, int item, long key)
{
    int		next, prev;
    qent_t	*q = &qp->q[0];
   
    if(item >= qp->size)
	return 0;

    qp->nr_entries++;
    next = q[QHEAD(qp)].qnext;
    while(q[next].qkey < key)
	next = q[next].qnext;
    q[item].qnext = next;
    q[item].qprev = prev = q[next].qprev;
    q[item].qkey = key;
    q[prev].qnext = item;
    q[next].qprev = item;

    return 1;
}

/**
 * getfirst - remove first item from queue.
 * @qp: queue pointer.
 *
 * Remove the first item from the queue.
 * Returns item value (index).
 */
int
getfirst(Q_t *qp)
{
    int		item;
    
    if((item = qp->q[QHEAD(qp)].qnext) < qp->size)
	return dequeue(qp, item);
    else
	return EMPTY;
}

/**
 * getlast - remove last item from queue.
 * @qp: queue pointer.
 *
 * Remove the last item from the queue.
 * Returns item value (index).
 */
int
getlast(Q_t *qp)
{
    int		item;

    if((item = qp->q[QTAIL(qp)].qprev) < qp->size && item >= 0)
	return dequeue(qp, item);
    else
	return EMPTY;
}


/**
 * insertd - insert a item into a delta list.
 * @qp: queue pointer
 * @item: item index (0 <= @item < queue size)
 * @key: sort key.
 *
 * Insert item in delta list order.  Items are stored in ascending order with
 * the keys are being stored as differences (deltas) from the preceeding key.
 *
 * Returns 1 if successful, otherwise 0.  
 */
int
insertd(Q_t *qp, int item, long key)
{
    int		next, prev;
    qent_t	*q = &qp->q[0];

    if(item >= qp->size)
	return 0;

    /* Hack to enable proper dequeuing from a delta list */
    qp->is_delta = 1;
    qp->nr_entries++;
    
    for(prev=QHEAD(qp),next=q[QHEAD(qp)].qnext;
	q[next].qkey < key; prev=next,next=q[next].qnext)
	key -= q[next].qkey;

    q[item].qnext = next;
    q[item].qprev = prev;
    q[item].qkey = key;
    q[prev].qnext = item;
    q[next].qprev = item;
    if(next < qp->size)
	q[next].qkey -= key;

    return 1;
}

#ifdef Q_TEST
#ifdef __TT8__
#include <tt8.h>
#include <tt8lib.h>
#endif
#include <assert.h>

int
main(int ac, char *av[])
{
    Q_t		*qp;
    int		i;
    
#ifdef __TT8__
    InitTT8(NO_WATCHDOG, TT8_TPU);
    SimSetFSys(16000000L);
#endif
    
    if((qp = newqueue(40)) == NULL)
    {
	fprintf(stderr, "Cannot allocate queue\n");
	exit(-1);
    }
    
    printf("FIFO test\n");
    i = 0;
    enqueue(qp, 5); i++;
    enqueue(qp, 10); i++;
    enqueue(qp, 1); i++;
    enqueue(qp, 39); i++;
    assert(enqueue(qp, 50) == 0);
    dump_queue(qp, stdout);
    
    printf("Dequeuing:\n");
    while(i--)
	printf("%d\n", getfirst(qp));

    clear_queue(qp);
    
    printf("Sorted list test\n");
    
    i = 0;
    insert(qp, i, 1000); i++;
    insert(qp, i, 500); i++;
    insert(qp, i, 200); i++;
    insert(qp, i, -42); i++;    
    insert(qp, i, 100); i++;    

    dump_queue(qp, stdout);
    i = i/2;
    while(i-- > 0)
	(void)getlast(qp);
    
    printf("Median: %ld\n", Qlastkey(qp));

    clear_queue(qp);

    printf("Delta list test\n");
    i = 0;
    insertd(qp, 1, 100); i++;
    insertd(qp, 2, 40); i++;
#if 0
    insertd(qp, 10, 500); i++;
    insertd(qp, 1, 200); i++;
    insertd(qp, 7, 50); i++;
    insertd(qp, 39, 50); i++;    
#endif    
    dump_queue(qp, stdout);
    printf("Removing index 1\n");
    
    dequeue(qp, 1);
    dump_queue(qp, stdout);
    i = getfirst(qp);
    printf("First index: %d\n", i);
    insertd(qp, 1, 1000);
    dump_queue(qp, stdout);
    i = getfirst(qp);
    printf("First index: %d\n", i);    
    dump_queue(qp, stdout);
    delete_queue(qp);
    
    return 0;
}

#endif
