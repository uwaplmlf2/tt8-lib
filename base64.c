/**@file
 *
 * arch-tag: 1fecb79f-5422-41d8-a950-77771cb5536d
 * $Id: base64.c,v ffbbeb6a435b 2007/10/10 19:52:11 mikek $
 *
 * Base-64 conversion functions.
 */
#include <stdio.h>
#include <stdlib.h>

#define PAD_CHAR	'='

/* Standard MIME base-64 encoding */
static const char *basis64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
"abcdefghijklmnopqrstuvwxyz"
"0123456789+/";

/**
 * Write a sequence of 1-3 bytes to a FILE as 4 base-64 encoded characters.
 * If the input sequence is less than 3 bytes, the encoded output will be
 * padded.
 *
 * @param  ofp  output FILE pointer
 * @param  data  buffer of bytes to encode.
 * @param  n  buffer length, 1-3.
 *
 * @return 1 if successful, 0 on error.
 */
int
b64_write(FILE *ofp, const unsigned char *data, int n)
{
    char		out[4];
    unsigned long	in;
    int			i, pad;

    if(n < 1)
	return 0;
    
    /*
    ** Left justify input bytes into a 24-bit integer.
    */
    in = 0L;
    pad = 3 - n;
    while(n--)
    {
	in = in << 8 | *data;
	data++;
	i++;
    }

    /* zero pad */
    in = in << (pad*8);

    /* Read out 6-bits at a time and encode */
    out[0] = basis64[(in >> 18) & 63];
    out[1] = basis64[(in >> 12) & 63];
    out[2] = basis64[(in >> 6) & 63];
    out[3] = basis64[in & 63];

    for(i = 0;i < (4-pad);i++)
	fputc(out[i], ofp);

    /* Pad character distingushes padding zeros from real zeros */
    for(;i < 4;i++)
	fputc(PAD_CHAR, ofp);
    
    return 1;
}

#ifdef TEST_B64
#include <string.h>

/*
** Test cases from http://en.wikipedia.org/wiki/Base64
*/
static char *input[] = {
    "Man",
    "sur",
    "su",
    "sure",
    "sure.",
    NULL};

static char *output[] = {
    "TWFu",
    "c3Vy",
    "c3U=",
    "c3VyZQ==",
    "c3VyZS4=",
    NULL};

int main(int ac, char *av[])
{
    int		i, n, rem, len;
    char	**inp, **outp;
    
    inp = input;
    outp = output;
    while(*inp)
    {
	printf("%s -> %s (", *inp, *outp);
	n = strlen(*inp);
	for(i = 0;i < n;i += 3)
	{
	    rem = n - i;
	    len = (rem < 3) ? rem : 3;
	    b64_write(stdout, &((*inp)[i]), len);
	}
	printf(")\n");
	inp++;
	outp++;
	
    }
    
    return 0;
}

#endif
