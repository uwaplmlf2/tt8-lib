/**@file
** $Id: tpuart.c,v 28180e1191d3 2008/06/10 01:12:00 mikek $
**
** Interface to the TPU-based UARTs on the TT8.
**
**
*/
#define USE_LPSLEEP	1
#include <stdio.h>
#include <stdlib.h>
#include <tt8lib.h>
#include <tpu332.h>
#include <tt8.h>
#include <tat332.h>
#include <sim332.h>
#include <qsm332.h>
#include <dio332.h>
#include <tt8pic.h>
#ifdef USE_LPSLEEP
#include "lpsleep.h"
#endif
#include "tpuart.h"

#define MAX_TPU	16
#define ESC	0x1b

/* Cache data structures to minimize malloc/free overhead */
static TPUart *ports[MAX_TPU];

/**
 * Open a pair of TPU channels for UART I/O.
 * Opens a pair of TPU channels for UART I/O.  par must be set to one
 * of the following constants defined in tpuart.h:
 * - TPU_NO_PARITY
 * - TPU_EVEN_PARITY
 * - TPU_ODD_PARITY
 *
 * @param  readpin  input channel (pin)
 * @param  writepin  output channel (pin)
 * @param  speed  baud rate
 * @param  par  parity, one of the following constants.
 * @param  dbits  number of data bits
 * @param  sbits  number of stop bits
 * @param  flags  I/O flags (see tpuart_set_flags())
 * @return pointer to a new TPUart structure or 0 if the open fails.
 */
TPUart*
tpuart_open(int readpin, int writepin, long speed, int par, 
	    int dbits, int sbits, int flags)
{
    TPUart	*t;
    int		r;


    if(readpin >= MAX_TPU || writepin >= MAX_TPU)
	return 0;

    /* Check the port cache */
    t = ports[readpin];
    
    if(t == NULL && (t = (TPUart*)malloc(sizeof(TPUart))) == NULL)
    {
#ifdef DEBUG
	printf("tpuart_open:  malloc failed\n");
#endif	
	return 0;
    }
    
    /* Cache the data structure */
    ports[readpin] = t;

    t->rchan = readpin;
    t->wchan = writepin;
    t->baud = speed;
    t->flags = flags;

    t->parity = (par == TPU_EVEN_PARITY) ? 'E' :
                  ((par == TPU_ODD_PARITY) ? 'O' : 0);

    t->dbits = dbits;
    
    TSerClose(t->rchan);
    TSerClose(t->wchan);

    if((r = TSerOpen(t->rchan, MiddlePrior, 0, (ptr)t->rbuf, TPU_BUF_SIZE, 
		     t->baud, t->parity, t->dbits, 1)) != tsOK ||
       (r = TSerOpen(t->wchan, MiddlePrior, 1, (ptr)t->wbuf, TPU_BUF_SIZE, 
		     t->baud, t->parity, t->dbits, 1)) != tsOK)
    {
#ifdef DEBUG
	printf("TSerOpen failed, error code = %d\n", r);
#endif
	free(t);
	ports[readpin] = NULL;
	
	return 0;
    }

#ifdef USE_LPSLEEP
    protect_tpu_pin(t->rchan);
    protect_tpu_pin(t->wchan);
#endif    
    return t;
}

/**
 * Close an open TPU uart.
 * Closes a TPU uart and frees all memory associated with it.  The data
 * structure must *not* be used after it is closed.
 *
 * @param  t  pointer to TPU uart
 */
void
tpuart_close(TPUart *t)
{
    TSerClose(t->rchan);
    TSerClose(t->wchan);

#ifdef USE_LPSLEEP
    unprotect_tpu_pin(t->rchan);
    unprotect_tpu_pin(t->wchan);
#endif
}

/**
 * Change the baud rate a TPU uart.
 *
 * @param  t  pointer to TPU uart.
 * @param  baud  new baud rate.
 */
void
tpuart_set_speed(TPUart *t, long baud)
{
    TSerResetBaud(t->rchan, baud);
    TSerResetBaud(t->wchan, baud);
    t->baud = baud;
}

/**
 * Set I/O flags
 * Set I/O flags for the TPU uart.  flags is a bitwise OR of one or
 * more of the following constants defined in tpuart.h:
 * - TPU_ONLCR (map CR to CRLF on output)
 * - TPU_IGNCR (ignore CR on input)
 * - TPU_ICRNL (map CR to LF on input)
 *
 * @param  t  pointer to a TPUart
 * @param  flags  I/O flags
 */
void 
tpuart_set_flags(TPUart *t, int flags)
{
    t->flags = flags;
}

/**
 * Set non-blocking input mode.
 * Set non-blocking input mode.  Reads will return immediately if
 * there is no data available.
 *
 * @param  t  pointer to a TPUart
 */
void
tpuart_set_noblock(TPUart *t)
{
    t->flags |= TPU_NOBLOCK;
}

/**
 * Set blocking input mode.
 * Set blocking input mode.  Reads will not return until data is
 * available.
 *
 * @param  t  pointer to a TPUart
 */
void
tpuart_set_block(TPUart *t)
{
    t->flags &= ~TPU_NOBLOCK;
}

/**
 * Send a BREAK signal.
 * Send a BREAK signal by temporarily closing the output channel of
 * the uart then setting the line low for 10ms, high for 350ms, and
 * then low for another 10ms.  The output channel is then reopened.
 *
 * @param  t  pointer to a TPUart
 */
void
tpu_send_break(TPUart *t)
{
    /*
    ** We must first disable the UART function on the write
    ** channel before we can change the state of the line.
    */
    TSerClose(t->wchan);

    TPUSetPin(t->wchan, 1);
    DelayMilliSecs(10L);
    TPUSetPin(t->wchan, 0);
    DelayMilliSecs(350L);
    TPUSetPin(t->wchan, 1);
    DelayMilliSecs(10L);

    TSerOpen(t->wchan, HighPrior, 1, (ptr)t->wbuf, TPU_BUF_SIZE, 
	     t->baud, t->parity, t->dbits, 1);
}

/**
 * Write bytes to the TPU uart.
 * Write up to n bytes from buf to the TPU uart.
 *
 * @param  t  pointer to a TPUart
 * @param  buf  array containing data to output.
 * @param  n  number of bytes to write.
 * @return number of bytes written.
 */
size_t
tpu_write(TPUart *t, const char *buf, size_t n)
{
    register int	c;
    register size_t	i;
    
    i = n;
    while(i--)
    {
	c = *buf++;
	c &= 0xff;
	
	if((t->flags & TPU_ONLCR) && c == '\n')
	    TSerPutByte(t->wchan, '\r');
	TSerPutByte(t->wchan, c);
    }
    
    return n;
}
    
/**
 * Read bytes from a TPU uart.
 * Read up to n bytes of data from the TPU uart and store in buf.
 *
 * @param  t  pointer to a TPUart
 * @param  buf  array to store input data.
 * @param  n  number of bytes to read.
 * @return number of bytes read.
 */
size_t
tpu_read(TPUart *t, char *buf, size_t n)
{
    register size_t	i;
    int			c;
    
    for(i = 0;i < n;i++)
    {
	if((t->flags & TPU_NOBLOCK) && !TSerByteAvail(t->rchan))
	    break;
	c = TSerGetByte(t->rchan);
	if(c == '\r')
	{
	    if((t->flags & TPU_IGNCR))
		continue;
	    else if((t->flags & TPU_ICRNL))
		c = '\n';
	}
	*buf++ = c;
    }
    
    return i;
}
	
    
/**
 * Character pass-thru function.
 * Pass-thru monitor function.  Reads characters from the console and
 * writes them to the TPU Uart and vice-versa.  Entering @stopchar from
 * the console will exit the function.
 *
 * @param  t  pointer to a TPUart.
 * @param  stopchar  character which will exit the function
 * @param  breakchar  character which will send a BREAK
 * @param  hook  if non-NULL, function to call on each read/write
 */
void
tpu_pass_thru(TPUart *t, int stopchar, int breakchar, void (*hook)(void))
{
    int		c;
    int		burst;
    
    while(1)
    {
	burst = 100;
	while(SerByteAvail() && burst--)
	{
	    c = SerGetByte();
	    if(c == breakchar)
		tpu_send_break(t);
	    else if(c == stopchar)
		return;
	    else
		TSerPutByte(t->wchan, c);
	}
	
	if(hook)
	    hook();
	
	burst = 100;
	while(TSerByteAvail(t->rchan) && burst--)
	    SerPutByte(TSerGetByte(t->rchan));
    }
}

/**
 * tpuart_inflush - flush all buffered input characters.
 * @t: pointer to a TPUart.
 *
 * Flush the input buffer of the TPU Uart.
 */
void
tpuart_inflush(TPUart *t)
{
    TSerInFlush(t->rchan);
}
