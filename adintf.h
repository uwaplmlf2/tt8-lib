/*
** $Id: adintf.h,v 4a9de31628cd 2007/04/20 21:53:14 mikek $
*/
#ifndef _ADINTF_H_
#define _ADINTF_H_

void set_ad_params(int chans[], int n);

#ifdef __GNUC__
/**
 * Read multiple channels of data from the A/D.
 * This function reads a single value from @n channels of the internal
 * A/D.  The A/D must be configured with set_ad_params() before calling
 * this function.
 *
 * @param  buf  buffer for storing the A/D values.
 * @param  n  number of channels.
 */
static __inline__ void read_ad_chans(unsigned short *buf, int n)
{
    unsigned short	*datap;
    
    /* Start the QSPI */
    *SPCR1 |= M_SPE;

    /* Wait for operation to complete */
    while(!(*SPSR & M_SPIF))
	;

    /* Stop the QSPI */
    *SPCR1 &= ~M_SPE;
    *SPSR &= ~M_SPIF;
    
    /*
    ** Read data from the QSPI receive RAM.  Note that the data starts
    ** at the second word.  The 12-bit A/D value is shifted left 3 bits
    ** so we shift it right before copying.
    */
    datap = &SPIRCV[1];
    while(n-- > 0)
	*buf++ = (*datap++ >> 3);
}

static __inline__ unsigned short read_ad(int chan)
{
    int			i, chans[8];
    unsigned short	x[8];
    
    for(i = 0;i < 8;i++)
	chans[i] = chan;
    set_ad_params(chans, 8);
    read_ad_chans(x, 8);
    return x[7];
}

#else
void read_ad_chans(unsigned short *buf, int n);
unsigned short read_ad(int chan);

#endif

#endif /* _ADINTF_H_ */
