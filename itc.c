/**@file
** $Id: itc.c,v 4a9de31628cd 2007/04/20 21:53:14 mikek $
**
** Interface to the Input Transition Counter TPU function.  The ITC
** function uses a 16 bit accumulator, this interface provides a
** cascade (wrap) counter which extends the effective counter width
** to 32 bits.
**
*/
#include <tt8.h>
#include <tt8lib.h>
#include <tpu332.h>

/* TPU function constants */

enum {CHAN_CTRL, LINK_CTRL, MAX_COUNT, TRANS_COUNT, 
      FINAL_TRANS_TIME, LAST_TRANS_TIME};

/* Host Service Request */
#define		Initialize		1

/* Host Sequence */
#define         SingleNoLinks		0
#define		ContinuousNoLinks	1
#define         SingleLinks		2
#define		ContinuousLinks		3

/* PAC Pin Action Control (input) */
#define         NoDetect	   	0x00
#define         DetRising               0x04
#define         DetFalling              0x08
#define         DetEither               0x0c
#define         NoChangePAC             0x10

#define NO_LINKS	0x0e

#define TPU_CHANNELS	16

static volatile unsigned long itc_counter[TPU_CHANNELS];
static volatile unsigned short itc_error[TPU_CHANNELS];

static unsigned short itc_active;

#define activate(c)	(itc_active |= (1 << (c)))
#define deactivate(c)	(itc_active &= ~(1 << (c)))
#define isactive(c)	(itc_active & (1 << (c)))

/*
 * Interrupt service routine.
 */
static void
itc_int_handler(void)
{
    ExcStackFrame	*sp = GetFramePtr();
    short		chan;

    /*
    ** TPU channel number is encoded in the vector offset.
    */
    chan = ((sp->vectofs & 0xfff) >> 2) - TPU_INT_VECTOR;
    if(chan >= 0 && chan < TPU_CHANNELS)
    {
	/*
	** Note that the ITC function will issue an interrupt when
	** the counter reaches 0xffff, *not* when it rolls over.
	*/
	itc_counter[chan] += 0x10000;
	itc_error[chan]++;
    }
    
    TPUClearInterrupt(chan);
}

/**
 * Stop an active counter
 * Stop a transition counter, mark as inactive and store the final
 * count. Returns 1 if successful, 0 if channel number is invalid.
 *
 * @param  chan  TPU channel number.
 * @return 1 if successful, 0 if channel number is invalid.
 */
int
itc_stop(short chan)
{
    if(chan < 0 || chan >= TPU_CHANNELS)
	return 0;
    
    if(isactive(chan))
    {
	TPUInterruptDisable(chan);
	itc_counter[chan] |= PRAM[chan][TRANS_COUNT];
	deactivate(chan);
    }
    return 1;
}

/**
 * Stop and clear an active counter.
 *
 * @param  chan  TPU channel number.
 * @return 1 if successful, 0 if channel number is invalid.
 */
int
itc_reset(short chan)
{
    int	r;
    
    if((r = itc_stop(chan)))
    {
	itc_counter[chan] = 0L;
	itc_error[chan] = 0;
    }
    
    return r;
}

/**
 * Read the current value of a transition counter.
 *
 * @param  chan  TPU channel number.
 * @return the current counter value.
 */
unsigned long
itc_read(short chan)
{
    unsigned long	c;
    
    if(chan < 0 || chan >= TPU_CHANNELS)
	return 0L;
    if(isactive(chan))
    {
	/*
	** If the counter is active, we must read the low word
	** from the TPU RAM.  If the wrap count (upper word)
	** changes during the OR operation, the values are read
	** again.
	*/
	c = itc_counter[chan] | PRAM[chan][TRANS_COUNT];
	if(itc_counter[chan] != (c & 0xffff0000L))
	    c = itc_counter[chan] | PRAM[chan][TRANS_COUNT];
    }
    else
	c = itc_counter[chan];

    return c - itc_error[chan];
}

/**
 * Start a transition counter.
 * Start a transition counter on the specified TPU channel.  The TPU ITC
 * function only provides a 16-bit counter.  This interface implements
 * a "rollover" counter to extend the range to 32-bits.
 *
 * @param  chan  TPU channel number.
 * @return 1 if successful, 0 if channel number is invalid.
 */
int
itc_start(short chan)
{
    static ExcCFrame	frame;
    
    if(!itc_reset(chan))
	return 0;
    
    TPUGetPin(chan);
    CHANPRIOR(chan, Disabled);
    TPUClearInterrupt(chan);

    InstallHandler(itc_int_handler, TPU_INT_VECTOR+chan, &frame);
    
    /*
    ** Select Input Transition Capture and count rising edges in
    ** continuous-mode.
    */
    FUNSEL(chan, ITC);
    PRAM[chan][CHAN_CTRL] = DetRising;
    PRAM[chan][LINK_CTRL] = NO_LINKS;
    PRAM[chan][MAX_COUNT] = 0xffff;
    HOSTSEQ(chan, ContinuousNoLinks);
    HOSTSERVREQ(chan, Initialize);
    CHANPRIOR(chan, HighPrior);
    while(HOSTSERVSTAT(chan))
	;
    TPUInterruptEnable(chan);
    activate(chan);
    
    return 1;
}
