/**@file
** $Id: ppwa.c,v 4a9de31628cd 2007/04/20 21:53:14 mikek $
**
** Interface to the TPUs PPWA function
*/
#include <stdio.h>
#include <tt8lib.h>
#include <stdlib.h>
#include <tpu332.h>
#include "ppwa.h"

#define ACCUM_RATE	0xff00
#define PPWA_Init	2

#define NoForceState	0x03
#define Cap1Match1	0x00
#define DetFalling	0x08

static volatile long	ppwa_count = 0;
static int		tpu_channel = -1;

/*
** TPU interrupt handler
*/
static void 
ppwa(void)
{
    long	ub, lw;

    ub = PRAM[tpu_channel][4] & 0xff;
    lw = PRAM[tpu_channel][5];
    PRAM[tpu_channel][4] = ACCUM_RATE;

    ppwa_count = ub << 16 | lw;
    TPUClearInterrupt(tpu_channel);
}

/**
 * Initialize the PPWA function on a TPU channel
 * Initialize the Period/Pulse Width Accumulator function on TPU channel
 * chan to count nr_periods of an input square wave.  
 *
 * @param  chan  TPU channel number (0 <= chan <= 15).
 * @param  nr_periods  number of periods to count (0 < nr_periods <= 255).
 * @return 1 if successful, or 0 if the parameters are invalid.
 */
int
ppwa_init(int chan, int nr_periods)
{
    static ExcCFrame	framebuf;    

    if(chan < 0 || chan > 15 || nr_periods < 1 || nr_periods > 255)
	return 0;
    
    tpu_channel = chan;
    ppwa_count = 0L;
    
    /*
    ** Set up the TPU channel to do PPWA.
    */
    TPUGetPin(chan);
    CHANPRIOR(chan, Disabled);
    TPUClearInterrupt(chan);
    InstallHandler(ppwa, TPU_INT_VECTOR+chan, &framebuf);
    
    FUNSEL(chan, PPWA);
    PRAM[chan][0] = NoForceState | DetFalling | Cap1Match1;
    PRAM[chan][1] = nr_periods << 8;
    PRAM[chan][4] = ACCUM_RATE;
    HOSTSEQ(chan, 0);

    HOSTSERVREQ(chan, PPWA_Init);
    CHANPRIOR(chan, LowPrior);
    while(HOSTSERVSTAT(chan))
	;
    TPUInterruptEnable(chan);

    return 1;
}

/**
 * Stop the PPWA measurement.
 */
void
ppwa_stop(void)
{
    if(tpu_channel < 0)
	return;
    CHANPRIOR(tpu_channel, Disabled);
    TPUInterruptDisable(tpu_channel);
    tpu_channel = -1;
}

/**
 * Check for valid accumulator value.
 *
 * @return true if the PPWA accumulator value is "fresh".
 */
int
ppwa_valid(void)
{
    return (tpu_channel >= 0 && (PRAM[tpu_channel][1] & 0xff) == 0);
}


/**
 * Read PPWA counts value.
 *
 * @return the most recent value from the PPWA accumulator or -1.
 */
long
ppwa_read(void)
{
    return (tpu_channel < 0) ? -1L : ppwa_count;
}
