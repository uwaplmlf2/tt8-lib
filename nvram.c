/**@file
** $Id: nvram.c,v 4a9de31628cd 2007/04/20 21:53:14 mikek $
**
** Functions to manage a non-volatile RAM based parameter table.  Each
** table entry consists of a 16-character (max) null-terminated string 
** (the key), an 8-byte value, and a type tag.  Each value may be either 
** a 7-character string, a long (4-byte) integer, or a double precision 
** floating-point number.
**
** A hash function is used to map the key to a table index.  Collisions
** are handled by linking.
**
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#ifdef __TT8__
#include <tt8lib.h>
#include <tpu332.h>
#include <tt8.h>
#include <tat332.h>
#include <sim332.h>
#include <qsm332.h>
#include <dio332.h>
#include <tt8pic.h>
#endif
#include "nvram.h"

#define NV_TABLESIZE	23	/* should be prime to minimize collisions */
#define NULL_LINK	(NV_TABLESIZE+1)
#define NV_START	0

#define NV_CHECK_INIT()	if(!initialized) nv_init(NULL, uee_read, uee_write)

typedef struct {
    int			(*read)(char*, int);
    int			(*write)(char*, int);
    int			(*init)(void);
    
    unsigned short	crc;
    unsigned short	nfree;
    nv_entry		e[NV_TABLESIZE];
} nv_table;

unsigned short crc(register char *block, register int n);

/*
** Return the offset in bytes of a structure field.
*/
#define toffset(f)	((unsigned)&(((nv_table*)NULL)->f))

static nv_table Nvram;
static int initialized = 0;
static int table_dirty = 0;

#define isempty(e)	((e).key[0] == '\0')

/*
 * Map a key name to a table index.
 */
static int
hash(register const char *name)
{
    unsigned long	sum = 0L;
    
    while(*name)
    {
	sum = (sum << 3) | (sum >> 29);
	sum ^= *name++;
    }
    
    return (sum % NV_TABLESIZE);
}

/*
 * Find an empty table slot.
 *
 */
static int
find_free_slot(void)
{
    register int	i = NV_TABLESIZE - 1;
    
    while(i-- >= 0)
	if(isempty(Nvram.e[i]))
	    return i;
    return -1;
}

static void
print_entry(nv_entry *e, void *data)
{
    printf("%-16s = ", e->key);
    switch(e->type)
    {
	case NV_CHARS:
	    printf("%-7s\n", e->value.c);
	    break;
	case NV_INT:
	    printf("%ld (0x%08lx)\n", e->value.l,
		   e->value.l);
	    break;
	case NV_FLOAT:
	    printf("%g\n", e->value.d);
	    break;
    }
}


/**
 * Initialize the non-volatile parameter table.
 * Initialize the non-volatile parameter table.  Each table entry consists of 
 * a 16-character (max) null-terminated string (the key), an 8-byte value, and
 * a type tag.  Each value may be either a 7-character string, a long (4-byte)
 * integer, or a double precision floating-point number.  A hash function is 
 * used to map the key to a table index.  Collisions are handled by linking.
 *
 * This function reads the table from non-volatile storage and verifies the 
 * CRC.  If the verification fails, an empty table is created.  All inserts
 * and removals are performed on the in-RAM copy of the table.  The user
 * must call nv_write() to update the non-volatile storage.
 *
 * @param  initf  nv-storage initialization function.
 * @param  readf  nv-storage read function
 * @param  writef  nv-storage write function.
 */
void
nv_init(int (*initf)(void), int (*readf)(char*,int), int (*writef)(char*,int))
{
    register nv_table	*t = &Nvram;
    register int	i;
    unsigned		ncrc, nread;

    if(initialized)
	return;
    
    t->init = initf;
    t->read = readf;
    t->write = writef;

    if(t->init)
	(*t->init)();
    ncrc  = sizeof(Nvram) - toffset(nfree);
    nread = sizeof(Nvram) - toffset(crc);
    
    if(!t->read || (*t->read)((char*)&(t->crc), nread) != nread ||
       t->crc != crc((char*)&(t->nfree), ncrc))
    {
	t->nfree = NV_TABLESIZE;
    
	for(i = 0;i < NV_TABLESIZE;i++)
	{
	    t->e[i].key[0] = '\0';
	    t->e[i].link = NULL_LINK;
	}
    }

    initialized = 1;
}

/**
 * Return number of table entries
 *
 * @return number of entries in the table.
 */
int
nv_size(void)
{
    return(NV_TABLESIZE - Nvram.nfree);
}

/**
 * Apply a function to each table entry.
 * Run the caller supplied callback function on each entry
 * of the table.
 *
 * @param  f callback function
 * @param  calldata  arbitrary data passed to the callback function.
 */
void
nv_foreach(nv_callback f, void *calldata)
{
    register nv_table	*t = &Nvram;
    register int	i;
    
    for(i = 0;i < NV_TABLESIZE;i++)
	if(!isempty(t->e[i]))
	    (*f)(&t->e[i], calldata);
}

/**
 * Write the table to non-volatile RAM.
 *
 * @return 1 if sucessful, otherwise 0.
 */
int
nv_write(void)
{
    unsigned	nbytes;

    if(!table_dirty)
	return 1;
    
    nbytes = sizeof(Nvram) - toffset(nfree);
    Nvram.crc = crc((char*)&(Nvram.nfree), nbytes);
    nbytes = sizeof(Nvram) - toffset(crc);
    
    if(Nvram.write)
	if((*Nvram.write)((char*)&(Nvram.crc), nbytes) != nbytes)
	    return 0;

    table_dirty = 0;
    
    return 1;
}

void
nv_show_all(void)
{
    nv_foreach(print_entry, NULL);
    printf("%d/%d slots free\n", Nvram.nfree, NV_TABLESIZE);
}

/**
 * Add a new entry to the parameter table.
 * Insert a new entry into the table.  If name already exists, the value
 * is updated to value.  type must be one of the following constants
 * defined in nvram.h:
 * - NV_CHARS (value is a character string, 7 chars max).
 * - NV_INT  (value is a 4-byte signed integer).
 * - NV_FLOAT (value is double-precision floating point). 
 *
 * If dowrite is not set, the user must call nv_write() at some later time to
 * update the non-volatile storage.
 *
 * @param  name  parameter key
 * @param  value  parameter value
 * @param  type  value type.
 * @param  dowrite  if non-zero force a write of the table to nv-storage.
 * @return index of the new entry or -1 if the  operation failed.
 */
int
nv_insert(const char *name, nv_value *value, int type, int dowrite)
{
    int			i = hash(name), j = NULL_LINK;
    register nv_table	*t = &Nvram;

    NV_CHECK_INIT();
    
    /* Table is full */
    if(t->nfree == 0)
	return -1;

    while(1)
    {
#ifdef TEST
	fprintf(stderr, "Checking index %d\n", i);
#endif	
	if(isempty(t->e[i]))
	{
	    /* Table slot is empty, insert new entry */
	    strncpy(t->e[i].key, name, NV_KEYLEN);
	    t->e[i].value = *value;
	    t->e[i].type  = type;
	    t->e[i].link  = NULL_LINK;
	    if(j != NULL_LINK)
		t->e[j].link = i;
	    t->nfree--;
	    break;
	}
	else if(!strncmp(t->e[i].key, name, NV_KEYLEN))
	{
	    /* Key match found, update the value */
	    t->e[i].value = *value;
	    t->e[i].type = type;
	    break;
	}
	else if(t->e[i].link == NULL_LINK)
	{
	    /* End of the list, add a new link */
	    j = i;
	    if((i = find_free_slot()) == -1)
		return -1;
	}
	else
	{
	    /* Walk the list */
	    j = i;
	    i = t->e[i].link;
	}
    }

    table_dirty++;
    
    if(dowrite)
	nv_write();
    
    return i;
}

/**
 * Locate the specified parameter.
 *
 * @param  name  parameter name
 * @return table index or -1 if not found.
 */
int
nv_locate(const char *name)
{
    register int	i = hash(name);
    register nv_table	*t = &Nvram;

    NV_CHECK_INIT();
    
    do
    {
	if(!strncmp(name, t->e[i].key, NV_KEYLEN))
	    return i;
	i = t->e[i].link;
    } while(i != NULL_LINK);
    
    return -1;
}

/**
 * Lookup parameter value.
 *
 * @param  name  parameter name
 * @param  value  pointer to returned value.
 * @return value type constant -1 if the parameter was not found.
 */
int
nv_lookup(const char *name, nv_value *value)
{
    register int	i;
    register nv_table	*t = &Nvram;

    NV_CHECK_INIT();
    
    if((i = nv_locate(name)) >= 0)
    {
	*value = t->e[i].value;
	return t->e[i].type;
    }
    
    return -1;
}

/**
 * Lookup parameter value by index.
 *
 * @param  idx  parameter index returned by nv_locate().
 * @param  value  pointer to returned value.
 * @return value type constant -1 if the parameter was not found.
 */
int
nv_index_lookup(int idx, nv_value *value)
{
    register nv_table	*t = &Nvram;

    if(idx < 0 || idx >= NV_TABLESIZE)
	return -1;

    *value = t->e[idx].value;
    return t->e[idx].type;    
}

/**
 * Remove a parameter from the table.
 * Remove the parameter specified by name from the table.  Note that
 * the parameter is only removed from the in-RAM copy.  The user must
 * call nv_write() to update the non-volatile storage.
 * 
 * @param  name  parameter name
 */
void
nv_delete(const char *name)
{
    register int	i = hash(name), j = NULL_LINK, k;
    register nv_table	*t = &Nvram;

    NV_CHECK_INIT();
    
    do
    {
	if(!strncmp(name, t->e[i].key, NV_KEYLEN))
	{
	    /*
	    ** We found the entry.  Invalidate the key by setting
	    ** its first character to '\0' and take care of the
	    ** "upstream" and "downstream" links.
	    */
	    t->e[i].key[0] = '\0';
	    if((k = t->e[i].link) != NULL_LINK)
	    {
		/* Copy the next list element into this slot */
		t->e[i] = t->e[k];
		t->e[k].key[0] = '\0';
	    }
	    else
	    {
		/* Set the link field of the previous element to NULL */
		if(j != NULL_LINK)
		    t->e[j].link = NULL_LINK;
	    }
	    t->nfree++;
	    table_dirty++;
	    nv_write();
	    
	    break;
	}
	/* Walk the list */
	j = i;
	i = t->e[i].link;
    } while(i != NULL_LINK);
}

/**
 * Read data from the NVRAM.
 * This function is a wrapper around the Onset Library function
 * UeeReadBlock and should be passed to nv_init() as the read handler.
 *
 * @param  dst  data buffer
 * @param  n  length of buffer.
 * @return number of bytes read.
 */
int
uee_read(char *dst, int n)
{
    return (UeeReadBlock(NV_START, dst, n) == ueeOk) ? n : 0;
}

/**
 * Write data to the NVRAM.
 * This function is a wrapper around the Onset Library function
 * UeeWriteBlock and should be passed to nv_init() as the write handler.
 *
 * @param  src  data buffer
 * @param  n  length of buffer.
 * @return number of bytes written.
 */
int
uee_write(char *src, int n)
{
    return (UeeWriteBlock(NV_START, src, n) == ueeOk) ? n : 0;
}



