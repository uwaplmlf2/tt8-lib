/*
** $Id: qdec.c,v 4a9de31628cd 2007/04/20 21:53:14 mikek $
**
** Interface to the Quadrature Decode TPU function.
**
**  
*/
#include <tt8.h>
#include <tt8lib.h>
#include <tpu332wa.h>
#include "qdec.h"

/* TPU function constants */

enum {CHAN_CTRL, LINK_CTRL, MAX_COUNT, TRANS_COUNT, 
      FINAL_TRANS_TIME, LAST_TRANS_TIME};

enum {EDGE_TIME, POSITION_COUNT, TCR1_VALUE, CHAN_PINSTATE,
      CORR_PINSTATE_ADDR, EDGE_TIME_LSB_ADDR};

/*
** Typedef and macro to access the Parameter RAM as an array of
** signed short integers rather than unsigned as used by Onset's
** include files.
*/
typedef short _SPRAM[8];
#define SPRAM	((_SPRAM *) (IMBASE + 0xf00))

/* Host Service Request */
#define		ITCInitialize		1
#define		QDECInitialize		3

/* Host Sequence */
#define         SingleNoLinks		0
#define		ContinuousNoLinks	1
#define         SingleLinks		2
#define		ContinuousLinks		3
#define		PrimaryChannel		0
#define 	SecondaryChannel	1

/* PAC Pin Action Control (input) */
#define         NoDetect	   	0x00
#define         DetRising               0x04
#define         DetFalling              0x08
#define         DetEither               0x0c
#define         NoChangePAC             0x10

#define NO_LINKS	0x0e

#define TPU_CHANNELS	16

#define ITC_OVERFLOW	6000

struct qdec {
    int			primary;
    int			secondary;
    volatile long	counter;
};

static struct qdec qdec_state[TPU_CHANNELS];
static unsigned short qdec_active;

#define activate(c)	(qdec_active |= (1 << (c)))
#define deactivate(c)	(qdec_active &= ~(1 << (c)))
#define isactive(c)	(qdec_active & (1 << (c)))

/*
 * Interrupt service routine for ITC channel.
 */
static void
itc_int_handler(void)
{
    ExcStackFrame		*sp = GetFramePtr();
    short			chan;
    register struct qdec	*qd;
    register short		counts, *reg;

    /*
    ** TPU channel number is encoded in the vector offset.
    */
    chan = ((sp->vectofs & 0xfff) >> 2) - TPU_INT_VECTOR;
    if(chan >= 0 && chan < TPU_CHANNELS)
    {
	qd = &qdec_state[chan];
	reg = &SPRAM[qd->primary][POSITION_COUNT];
	
	/*
	** Read the position-count register from the primary QDEC channel
	** and reset it to zero.  Any counts occurring between these two
	** instructions will be lost.
	*/
	counts = *reg;
	*reg = 0;

	qd->counter += counts/4;
    }
    
    TPUClearInterrupt(chan);
}

/**
 * qdec_start - start a QDEC counter on a TPU channel.
 * @chan: TPU channel number for the ITC function
 * @pri: the primary QDEC TPU channel.
 * @sec: the secondary QDEC TPU channel.
 *
 * Initialize the Quadrature Decode TPU function.  The QDEC counter may be
 * incremented or decremented depending on the relative phase of the signal on
 * @pri and @sec.  When @pri is leading, the counter is incremented and when
 * @sec is leading, the counter is decremented.
 *
 * Unfortunately, the QDEC TPU
 * function will not generate an interrupt when its counter overflows or
 * underflows.  Effective use of the QDEC function to monitor a motor
 * encoder requires that one of the encoder signals also be input to a third
 * TPU channel which will run the ITC function, @chan.  The ITC function can be
 * configured to generate an interrupt when the transition counter exceeds a
 * specified maximum value (MAX_COUNT).  By setting MAX_COUNT, to a value less
 * than 32767/4 (the QDEC counter is 4x the number of transitions) we can
 * insure that an ITC interrupt will be generated before the QDEC counter
 * overflows or underflows.  The ITC interrupt handler is responsible for
 * reading and then zeroing the QDEC counter.
 *
 * Note that @chan is the channel number which should be used for all counter
 * operations (i.e. calls to qdec_read() etc.).
 *
 * Returns 1 if successful, 0 if channel number is invalid.
 */
int
qdec_start(short chan, short pri, short sec)
{
    static ExcCFrame	frame;
    
    if(!qdec_reset(chan))
	return 0;
    
    TPUGetPin(chan);
    TPUGetPin(pri);
    TPUGetPin(sec);
    
    CHANPRIOR(chan, Disabled);
    CHANPRIOR(pri, Disabled);
    CHANPRIOR(sec, Disabled);

    TPUClearInterrupt(chan);

    InstallHandler(itc_int_handler, TPU_INT_VECTOR+chan, &frame);

    /*
    ** Initialize the primary and secondary channels for the
    ** Quadrature Decode function.
    */

    FUNSEL(pri, QDEC);
    FUNSEL(sec, QDEC);
    
    PRAM[pri][CORR_PINSTATE_ADDR] = (sec << 4) + CHAN_PINSTATE*2;
    PRAM[sec][CORR_PINSTATE_ADDR] = (pri << 4) + CHAN_PINSTATE*2;
    PRAM[pri][EDGE_TIME_LSB_ADDR] = (pri << 4) + 1;
    PRAM[sec][EDGE_TIME_LSB_ADDR] = PRAM[pri][EDGE_TIME_LSB_ADDR];
    PRAM[pri][POSITION_COUNT] = 0;
    HOSTSEQ(pri, PrimaryChannel);
    HOSTSEQ(sec, SecondaryChannel);
    
    HOSTSERVREQ(pri, QDECInitialize);
    HOSTSERVREQ(sec, QDECInitialize);
    CHANPRIOR(pri, HighPrior);
    CHANPRIOR(sec, HighPrior);
    while(HOSTSERVSTAT(pri) || HOSTSERVSTAT(sec))
	;
    /*
    ** Select Input Transition Capture and count rising edges in
    ** continuous-mode.
    */
    FUNSEL(chan, ITC);
    PRAM[chan][CHAN_CTRL] = DetRising;
    PRAM[chan][LINK_CTRL] = NO_LINKS;
    PRAM[chan][MAX_COUNT] = ITC_OVERFLOW;
    HOSTSEQ(chan, ContinuousNoLinks);
    HOSTSERVREQ(chan, ITCInitialize);
    CHANPRIOR(chan, HighPrior);
    while(HOSTSERVSTAT(chan))
	;

    /*
    ** Initialize state data structure and enable interrupts
    */
    qdec_state[chan].primary = pri;
    qdec_state[chan].secondary = sec;
    qdec_state[chan].counter = 0L;

    TPUInterruptEnable(chan);    
    activate(chan);
    
    return 1;
}

/**
 * qdec_stop - stop a QDEC counter.
 * @chan: TPU channel
 *
 * Stop a QDEC counter, mark as inactive and save the final
 * count.  Returns 1 if successful, 0 if channel number is invalid.
 */
int
qdec_stop(short chan)
{
    register struct qdec	*qd;
    
    if(chan < 0 || chan >= TPU_CHANNELS)
	return 0;
    
    if(isactive(chan))
    {
	qd = &qdec_state[chan];
	TPUInterruptDisable(chan);
	qd->counter += SPRAM[qd->primary][POSITION_COUNT]/4;
	deactivate(chan);
    }
    return 1;
}

/**
 * qdec_reset - stop and clear a QDEC counter.
 * @chan: TPU channel
 *
 * Stop an active QDEC counter and clear it.
 * Returns 1 if successful, 0 if channel number is invalid.
 */
int
qdec_reset(short chan)
{
    int	r;
    
    if((r = qdec_stop(chan)))
	qdec_state[chan].counter = 0L;
    
    return r;
}

/**
 * qdec_read - read the current QDEC counter value.
 * @chan: TPU channel.
 *
 * Read the current value of a QDEC counter.
 * Returns current value if successful, 0 if channel number is invalid.
 */
long
qdec_read(short chan)
{
    long			c;
    int				intmask;
    register short		*reg;
    register struct qdec	*qd;
    
    if(chan < 0 || chan >= TPU_CHANNELS)
	return 0L;
    if(isactive(chan))
    {
	qd = &qdec_state[chan];
	reg = &SPRAM[qd->primary][POSITION_COUNT];
	intmask = GetInterruptMask();
	SetInterruptMask(NO_RUPTS_MASK);
        c = qd->counter + *reg/4;
	SetInterruptMask(intmask);
    }
    else
	c = qdec_state[chan].counter;

    return c;
}

