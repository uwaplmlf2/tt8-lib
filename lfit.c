/**@file
** $Id: lfit.c,v 4a9de31628cd 2007/04/20 21:53:14 mikek $
**
** Line fitting functions.  Useful for sensor calibrations.
**
*/

#define	X	0
#define Y	1
#define XY	2

/*
 * calc_ss - calculate ssxx, ssyy, and ssxy.
 */
static void
calc_ss(double x[], double y[], int n, double ss[], double avg[])
{
    double	sumx = 0., sumy = 0., sumxy = 0., sumxx = 0., sumyy = 0.;
    double	xbar, ybar;
    int		i;
    
    for(i = 0;i < n;i++)
    {
	sumx += x[i];
	sumy += y[i];
	sumxx += (x[i]*x[i]);
	sumyy += (y[i]*y[i]);
	sumxy += (x[i]*y[i]);
    }
    
    avg[X] = xbar = sumx/(double)n;
    avg[Y] = ybar = sumy/(double)n;
    ss[X] = sumxx - 2*xbar*sumx + n*xbar*xbar;
    ss[Y] = sumyy - 2*ybar*sumy + n*ybar*ybar;    
    ss[XY] = sumxy - ybar*sumx - xbar*sumy + n*xbar*ybar;
}

/**
 * Fit a line to a series of x,y data points.
 * Calculates the coefficients of the best-fit line through the data
 * points specified by @x and @y: 
 *
 *	\f$y = c[0] + c[1]*x\f$
 *
 * @param  x  vector of X coordinates.
 * @param  y  vector of Y coordinates.
 * @param  n  length of @x and @y.
 * @param  c  returned linear equation coefficients.
 * @return the correlation coefficient, R.
 */
double
lfit(double x[], double y[], int n, double c[])
{
    double	ssr, d, yhat;
    double	ss[3], avg[2];
    int		i;

    calc_ss(x, y, n, ss, avg);
    c[1] = ss[XY]/ss[X];
    c[0] = avg[Y] - c[1]*avg[X];

    ssr = 0.;
    for(i = 0;i < n;i++)
    {
	yhat = c[0] + c[1]*x[i];
	d = (yhat - avg[Y]);
	ssr += d*d;
    }
    
    return ssr/ss[Y];
}
