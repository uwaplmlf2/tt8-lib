;;;
;;; $Id: adint.asm,v 1.4 2000/03/23 18:53:05 mike Exp $
;;; 
;;; A/D interrupt handler.  Borrowed from Onset's example code
;;; with some minor modifications.
;;;
;;; Reads a block of 8 consecutive words from the QSPI receive
;;; queue (Receive Data RAM) and writes them into the current
;;; storage location pointed to by _adbufp.  _adbuflen specifies
;;; the number of words left to read.
;;;
	far code
	far data
	dseg
	public	__adbufp,__adbuflen
	ds 0
.firstw	ds.w	1	
	cseg
	public _ad_int_handler
_ad_int_handler:
	;; Read the first word from the queue
	;; before it gets overwritten.
	move.w	$FFFD00,.firstw
	movem.l	a0-a1/d0,-(sp)		; save used registers

	move.l	__adbufp,a0		; load buffer pointer
        move.l  __adbuflen,d0		; load buffer length
        subi.l  #8,d0			; is the buffer full?
        blt     .done
					    
	lea	$FFFD00,a1
	move.l	(a1)+,(a0)+		; read two words at a time
	move.l	(a1)+,(a0)+	
	move.l	(a1)+,(a0)+	
	move.l	(a1)+,(a0)+
	move.w	.firstw,-16(a0)		; re-insert first word
	move.l	a0,__adbufp
        move.l  d0,__adbuflen
.done
	;; Read and write SPSR to clear the interrupt
	tst.b	$FFFC1F
	move.b	#0,$FFFC1F

	movem.l	(sp)+,a0-a1/d0		; restore used registers

	rte
	end