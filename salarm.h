/*
**$Id: salarm.h,v 4a9de31628cd 2007/04/20 21:53:14 mikek $
*/
#ifndef _SALARM_H_
#define _SALARM_H_


#define ALARM_ONESHOT	1
#define ALARM_REPEAT	0

int alarm_set_ms(long msecs, int type, void (*action)(void));
void alarm_reset(void);
void alarm_shutdown(void);

#define alarm_set(s, t, a)	alarm_set_ms((s)*1000L, t, a)
#endif
