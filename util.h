/*
** $Id: util.h,v 4a9de31628cd 2007/04/20 21:53:14 mikek $
*/
#ifndef _UTIL_H_
#define _UTIL_H_

#define DUMP_OK		0
#define DUMP_ERROR	-1
#define DUMP_RESTART	1

void backtrace(void);
void default_handler(void);
int yes_or_no(const char *prompt, int def);
int  fileexists(const char *name);
int dump(const char *filename);

#endif /* _UTIL_H_ */
