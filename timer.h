/*
** $Id: timer.h,v 4a9de31628cd 2007/04/20 21:53:14 mikek $
*/
#ifndef _TIMER_H_
#define _TIMER_H_

typedef int (*tfunc)(void);

int init_timer(void);
void shutdown_timer(void);
int set_timer(long secs, tfunc callback);
void unset_timer(int id);
void unset_all_timers(void);
void suspend_timers(void);
void resume_timers(void);

#endif /* _TIMER_H_ */
