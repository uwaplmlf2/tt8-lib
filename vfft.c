/*
** $Id: vfft.c,v 4a9de31628cd 2007/04/20 21:53:14 mikek $
**
** Complex to complex 1D FFT based on the Numerical Recipies implementation.
**
**
*/
#include <math.h>

#define TABLE_SIZE	16

#define FSWAP(a,b) 	do {double t = (a);(a) = (b);(b) = t;} while(0)



static double sine_table[TABLE_SIZE];

/**
 * build_sine_table - build a table of FFT coefficients.
 *
 * Initialize a table of sine coefficients for cfft().
 */
void
build_sine_table()
{
    double		pi = atan2(1., 0.)*2.;
    register int	i;
    register long	div;

    /*
    ** sine_table[i] <-- sin(2*pi/(2^i))
    */
    sine_table[0] = 0.;
    sine_table[1] = 0.;
    sine_table[2] = 1.;
    for(i = 3,div = 4;i < TABLE_SIZE;i++,div <<= 1)
	sine_table[i] = sin(pi/div);
}


/**
 * cfft - complex-to-complex 1D FFT.
 * @data: pointer to array of complex data.  Real values
 *		        are stored in the even indicies and imaginary
 *			values are stored in the odd.  Will contain the
 *                      transformed data on return.
 * @nl2: log2 of length of @data (number of complex values).
 * @isign: 1 for forward transform, -1 for inverse.
 *
 * Perform an in-place 1-D complex-to-complex FFT.  This is essentially the
 * Numerical Recipies implementation.
 */
void
cfft(float *data, int nl2, int isign)
{
    double		wr, wi, wpr, wpi, wtemp;
    double		tempr, tempi, re_i, im_i, re_j, im_j;
    long		n, istep;
    register long	i, j, m, ex, mmax;
    register DTYPE	*x_i, *x_j;
    
    
    /* Number of real values --> n */
    n = 1L << (nl2 + 1);

    /* 
    ** This fft algorithm assumes a 1-based array index.  Adjust
    ** the array pointer.
    */
    data--;

    /* Bit reverse and swap */    
    j = 1;
    for(i = 1;i < n;i += 2)
    {
	if(j > i)
	{
	    FSWAP(data[i], data[j]);
	    FSWAP(data[i+1], data[j+1]);
	}
	m = n >> 1;
	while(m >= 2 && j > m)
	{
	    j -= m;
	    m >>= 1;
	}
	j += m;
    }

    /*
    ** Loop over powers of 2 from 2 to the dimension of
    ** the input array.
    */
    ex = 1;
    while(ex <= nl2)
    {
	/*
	** Use a table lookup for the sine values, the index is
	** the base-2 exponent.
	*/
	mmax  = 1L << ex;
	istep = mmax << 1;
	wpi   = sine_table[ex] * isign;  /* sin(-x) = -sin(x) */
	ex++;
	wtemp = sine_table[ex] * isign;

	wpr   = -2.*wtemp*wtemp;
	wr    = 1.;
	wi    = 0.;
	for(m = 1;m < mmax;m += 2)
	{
	    x_i = data + m;
	    x_j = x_i + mmax;
	    
	    for(i = m;i <= n;i += istep)
	    {
		/*
		** Use temporaries to maximize register lifetime and
		** minimize memory access.
		*/
		re_j = x_j[0];
		im_j = x_j[1];
		re_i = x_i[0];
		im_i = x_i[1];
		
		tempr = re_j*wr - im_j*wi;
		tempi = im_j*wr + re_j*wi;
		re_j  = re_i - tempr;
		re_i  += tempr;
		im_j  = im_i - tempi;
		im_i  += tempi;

		x_j[0] = re_j;
		x_j[1] = im_j;
		x_i[0] = re_i;
		x_i[1] = im_i;

		x_i += istep;
		x_j += istep;
	    }
	    wtemp = wr;
	    wr    = wr*wpr - wi*wpi + wr;
	    wi    = wi*wpr + wtemp*wpi + wi;
	}
    }
    
}

#ifdef TEST
#ifdef __TT8__
#include <tt8.h>
#include <tt8lib.h>
#include <tat332.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define FFTLEN_EX	12

#ifndef __TT8__
#define	ttmnow	clock
#endif

void main(void)
{

    float	*datastart;
    double	pi = acos(-1);
    long    i;
    ulong	index;
    int n=1 << FFTLEN_EX;
#ifdef __TT8__
    time_tt before,after;
#else
    clock_t	before, after;
#endif    
    long dt;
    float pwr=0;

#ifdef __TT8__
    InitTT8(NO_WATCHDOG,TT8_TPU);
#endif

    datastart = calloc(n*2,sizeof(float));

    for(i=0; i<n; i++) 
    {
	datastart[i*2]= 16*(sin(6*pi/n*i)+ cos(20*pi/n*i));
	datastart[i*2+1]=0;
	pwr += datastart[i*2]*datastart[i*2];
    } 

    fprintf(stderr, "power = %lg\n",pwr);

    build_sine_table();

    fprintf(stderr, " doing fft ...\n");
    before = ttmnow();
    cfft(datastart, FFTLEN_EX, 1);
    after = ttmnow();

    /* How long did the FFT take */
#ifdef __TT8__
    dt = ttmcmp(after, before);
    printf("dt = (%ld s, %ld ticks)\n", dt/GetTickRate(),dt%GetTickRate());
#else
    dt = after - before;
    fprintf(stderr, "FFT time: (%ld s, %ld ticks)\n", dt/CLOCKS_PER_SEC,
	    dt%CLOCKS_PER_SEC);
    for(i = 0;i < n;i += 2)
	printf("%g\t%g\n", datastart[i], datastart[i+1]);
#endif
	
#ifdef __TT8__
    Reset();
#endif
    exit(0);
}
#endif






