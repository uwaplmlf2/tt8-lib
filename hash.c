/**@file
** $Id: hash.c,v 6d41b1b39093 2008/04/21 19:34:02 mikek $
**
** HashTable object functions.  A HashTable associates names (char strings)
** with arbitrary blocks of data.
**
*/
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stddef.h>
#include <time.h>
#include <tt8lib.h>

#include "hash.h"

#define MIN(a,b)	((a) < (b) ? (a) : (b))

#ifdef AZTEC_C
static int
strcasecmp(const char *s1, const char *s2)
{
    while(*s1 != '\0' && tolower(*s1) == tolower(*s2))
    {
	s1++;
	s2++;
    }
    
    return tolower(*(unsigned char *) s1) - tolower(*(unsigned char *) s2);
}
#endif
/*
 * Allocate memory for a new table cell.  The cell is allocated as a
 * single block with one call to malloc.
 */
static struct elem*
alloc_node(size_t namelen, short datalen)
{
    register struct elem	*e;
   
    /*
    ** Round the length of the name up to the next longword boundary.
    */
    namelen = (namelen + 3) & ~3;
    if((e = (struct elem*)malloc(sizeof(struct elem) + namelen + datalen)))
	e->data = (char*)e + sizeof(struct elem) + namelen;
    return e;
}

static int
hash(long base, register const char *name)
{
    unsigned long	sum = 0L;
    
    while(*name)
    {
	sum = (sum << 3) | (sum >> 29);
	sum ^= *name++;
    }
    
    return (sum % base);
}

static int
hash_nocase(long base, const char *name)
{
    unsigned long	sum = 0L;
    char		c;
    
    while((c = *name++))
    {
	if(isalpha(c))
	    c = tolower(c);
	
	sum = (sum << 3) | (sum >> 29);
	sum ^= c;
    }
    
    return (sum % base);
}

static struct elem*
lookup(HashTable *htp, const char *name)
{
    register struct elem	*e;

    /*
    ** The most recent lookup is cached in HashTable.last.  This
    ** allows calling elem_exists() followed by find_elem() on
    ** the same key without the performance hit of a second lookup.
    */
    if(htp->last && htp->cmp(name, htp->last->name) == 0)
	return htp->last;
    
    for(e = htp->e[htp->func((long)htp->ncells, name)];e != NULL;e = e->next)
	if(htp->cmp(name, e->name) == 0)
	    break;
    return (htp->last = e);
}

/**
 * Create a new HashTable.
 * Create a HashTable data structure to associate strings (names) with
 * arbitrary blocks of data.  The return value is a pointer to a new HashTable
 * or NULL if memory allocation fails.  @type must be set to one of the
 * following constants:
 * - HT_STANDARD (key lookups are case sensitive)
 * - HT_NOCASE (key lookups are case insensitive)
 *
 * @param  size  number of table entries.
 * @param  type  determines how key lookups are handled.
 * @return a new HashTable instance.
 */
HashTable*
ht_create(unsigned size, int type)
{
    register HashTable	*htp;
    ulong		nbytes;
    
    nbytes = sizeof(HashTable) + (size-1)*sizeof(struct elem*);
    
    if((htp = malloc(nbytes)) != NULL)
    {
	htp->ncells = size;
	if(type == HT_NOCASE)
	{
	    htp->func = hash_nocase;
	    htp->cmp = strcasecmp;
	}
	else
	{
	    htp->func = hash;
	    htp->cmp = strcmp;
	}

	htp->last = 0;
	memset(htp->e, 0, size*sizeof(struct elem*));
    }
    
    return htp;
}

/**
 * Destroy a HashTable instance.
 * Free all memory associated with a HashTable.
 *
 * @param  htp  pointer to HashTable.
 */
void
ht_destroy(HashTable *htp)
{
    register unsigned		i = 0;
    register struct elem	*p, *q;
    
    for(i = 0;i < htp->ncells;i++)
    {
	if(htp->e[i])
	{
	    /*
	    ** Free the chain rooted at this index
	    */
	    p = htp->e[i];
	    q = 0;
	    while(p)
	    {
		q = p->next;
		free(p);
		p = q;
	    }
	}
    }
    
    free(htp);
}


/**
 * Insert a new element in the table.
 * This function associates a name with an arbitrary block of data pointed
 * to by what.  If len is greater than zero, the data is copied into the 
 * hash table.
 *
 * @param  htp  pointer to HashTable
 * @param  name  name of the element.
 * @param  what  opaque data handle to associate with @name.
 * @param  len  size (in bytes) of the data.
 * @return 1 if successful, 0 if memory allocation fails.
 */

int
ht_insert_elem(HashTable *htp, const char *name, const void *what, 
	     unsigned short len)
{
    struct elem		*e;
    int			hval;

    e = lookup(htp, name);
    /*
     * If the entry exists but is not large enough to hold the
     * new value, we must remove it.
     */
    if(e != NULL && e->len < len)
    {
	ht_remove_elem(htp, name);
	e = NULL;
    }
    
    if(e == NULL)
    {
	if((e = alloc_node(strlen(name), len)) == NULL)
	    return 0;
	strcpy(e->name, name);
	hval = htp->func((long)htp->ncells, name);
	e->next = htp->e[hval];
	htp->e[hval] = e;
    }

    if(len > 0)
	memcpy(e->data, what, (size_t)len);
    else
	e->data = (void*)what;

    e->len = len;    
    return 1;
}


/**
 * Remove a named element from the table.
 * Removes the element and frees all associated memory.
 *
 * @param  htp  pointer to HashTable.
 * @param  name  element name.
 */
void
ht_remove_elem(HashTable *htp, const char *name)
{
    register struct elem	*p, *q;
    int				hval = htp->func((long)htp->ncells, name);
    
    q = 0;
    for(p = htp->e[hval];p != NULL;q = p,p = p->next)
	if(htp->cmp(name, p->name) == 0)
	{
	    if(q == 0)
		htp->e[hval] = p->next;
	    else
		q->next = p->next;
	    free(p);
	    break;
	}
}

/**
 * Determine whether a named element is in the table.
 *
 * @param  htp  pointer to HashTable.
 * @param  name  name of the element.
 * @return true if the element exists otherwise false.
 */
int
ht_elem_exists(HashTable *htp, const char *name)
{
    return (lookup(htp, name) != NULL);
}


/**
 * Retrieve an element from the table.
 * Find the element associated with name and return the data.  If the
 * data had actually been stored in the table (i.e. not just a pointer)
 * and lenp is non-NULL, the data will be copied into whatp.  Returns
 * 1 if the element was found or 0 if not found.
 
 * @param  htp  pointer to HashTable.
 * @param  name  name of the element.
 * @param  whatp  returned data handle (if non-NULL)
 * @param  lenp  will contain the data size on return (if non-NULL).
 * @return true if the element exists otherwise false.
 */
int
ht_find_elem(HashTable *htp, const char *name, void **whatp, 
	   unsigned short *lenp)
{
    struct elem		*e;
    
    if((e = lookup(htp, name)) != NULL)
    {
	if(whatp)
	{
	    if(e->len > 0 && lenp)
		memcpy(*whatp, e->data, (size_t)MIN(e->len, *lenp));
	    else
		*whatp = e->data;
	}
	
	if(lenp)
	    *lenp = e->len;

	return 1;
    }
    
    return 0;
}


/**
 * Call a supplied function for each entry in the table.
 * Iterate over all elements of the table, calling the supplied function
 * and passing a pointer to the element as an argument.
 *
 * @param  htp  pointer to HashTable.
 * @param  func  function to apply to each entry.
 * @param  calldata  arbitrary data passed to func as second argument.
 */
void
ht_foreach_elem(HashTable *htp, ht_callback func, void *calldata)
{
    register unsigned		i = 0;
    register struct elem	*e;
    
    for(i = 0;i < htp->ncells;i++)
    {
	if((e = htp->e[i]))
	    while(e)
	    {
		(*func)(e, calldata);
		e = e->next;
	    }
    }
}

