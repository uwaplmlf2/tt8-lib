/*
** $Id: adburst.c,v 4a9de31628cd 2007/04/20 21:53:14 mikek $
**
** Sample a single A/D channel in "burst" mode.
**
*/
#include <tt8.h>
#include <tat332.h>
#include <sim332.h>
#include <qsm332.h>
#include <dio332.h>
#include <tt8lib.h>

#define MAX186_BAUD	4

#define burst_start()	(*SPCR1 |= M_SPE)
#define burst_stop()	(*SPCR1 &= ~M_SPE)

void ad_int_handler(void);

/* A/D Channel selection codes */
static const int ADUnipolar[8] = {
    0x8f, 0xcf, 0x9f, 0xdf, 0xaf, 0xef, 0xbf, 0xff };
static const int ADBipolar[8] = {
    0x87, 0xc7, 0x97, 0xd7, 0xa7, 0xe7, 0xb7, 0xf7 };

/* Used by interrupt handler */
short *_adbufp;
volatile long _adbuflen;


/**
 * set_burst_params - Initialize the A/D channel for "burst" mode.
 * @chan: A/D channel code.
 * @period: desired sampling rate in microseconds.
 *
 * Trys to set the sampling period as close as possible to @period
 * microseconds.  This function assumes that the system clock is
 * set to 16Mhz, this limits the allowable values for @period.
 *
 *	10 <= @period <= 510
 *
 * The return value is the actual sampling period in microseconds
 */
static int
set_burst_params(int chan, int period)
{
    /*
    ** Delay parameters.  DSCKL specifies the delay from chip-select
    ** valid to the first clock transition for the serial transfer.
    ** DTL specifies the delay after each serial transfer.  We use
    ** a 2Mhz baud rate and a transfer size of 16 bits so each transfer
    ** requires 8 usecs.  Refer to Chapter 5 of the MC68332 User's
    ** Manual.
    */
    int			dsckl, dtl;
    int			delay_required;
    unsigned		mask;
    register int	i;
    
    if(period <= 10)
    {
	period = 10;
	dsckl = 15;
	dtl   = 0;
    }
    else if(period == 11)
    {
	dsckl = 16;
	dtl   = 1;
    }
    else
    {
	delay_required = period - 10;
	dsckl = 32;
	dtl = (delay_required * 16)/32;
	if(dtl > 255)
	    dtl = 255;
    }

    /*
    ** The MAX186 is on PCS3.  The device is disabled when CS3 is set.
    */
    *QPDR |= (M_PCS3 | M_SCK | M_MISO);
    *QPAR |= (M_MOSI | M_MISO | M_PCS3);

    /* All chip-selects, MOSI and SCK are outputs */
    *QDDR = M_PCS3 | M_PCS2 | M_PCS1 | M_PCS0 | M_SCK | M_MOSI;
    
    /*
    ** Set QSPI control registers
    */

    /* Specify baud rate and set Master Mode */
    *SPCR0 = MAX186_BAUD | (M_MSTR & SET);

    /* Command queue parameters */
    *SPCR2 = 15			/* Starting queue pointer (15) */
      | (7 << 8)		/* Ending queue pointer (7) */
      | (M_SPIFIE & SET)	/* Enable interrupt when queue is full */
      | (M_WREN & SET)		/* Enable queue wrap-around */
      | (M_WRTO & CLR);		/* Wrap to 0 */

    /* Disable Loop Mode, HALTA interrupts, and Halt */
    *SPCR3 = 0;

    /* Set delays */
    *SPCR1 = ((dsckl & 0x7f) << 8) | (dtl & 0xff);
    
    /*
    ** Load the transmit registers with the A/D channel
    ** select code.
    */
    SPIXMT[15] = chan;
    for(i = 0;i < 8;i++)
	SPIXMT[i] = chan;

    /*
    ** Load the command registers
    */

    /* chip select mask */
    mask = (M_CS0 & CLR) | (M_CS1 & CLR) | (M_CS2 & CLR) | (M_CS3 & CLR);
    
    SPICMD[15] = (M_CONT & SET) | (M_DT & (dtl ? SET : CLR)) | mask;
    SPICMD[0] = (M_CONT & SET)
      | (M_BITSE & SET)
      | (M_DT & (dtl ? SET : CLR))
      | (M_DSCK & SET) | mask;
    for(i = 1;i < 8;i++)
	SPICMD[i] = SPICMD[0];
    
    /* Clear interrupt flag */
    *SPSR &= ~M_SPIF;
    

    /*
    ** Calculate and return the actual sampling period.
    */
    if(dtl)
	period = 8 + (dsckl + 32*dtl)/16;
    else
	period = 8 + (dsckl + 17)/16;
    
    return period;
}

/**
 * adburst - Read a series of samples from an A/D channel.
 * @chan: A/D channel number (0-7).
 * @bipolar: if non-zero, sample in bipolar mode.
 * @period: desired sampling period in microseconds.
 * @n: number of samples to read.  
 * @databuf: buffer to hold the sampled data.
 *
 * This function reads a series of samples from the MAX186 A/D converter
 * in high-speed (burst) mode.  Due to the method used to read the samples,
 * @n should be a multiple of 8.  The value of @n will be truncated to the 
 * nearest multiple of 8 before sampling.
 *
 * The return value is the actual sampling period in microseconds (which may 
 * differ from @period) or zero if an error occurs.
 */
int
adburst(int chan, int bipolar, int period, long n, short *databuf)
{
    int			p;
    long		clkf;
    
    if(chan < 0 || chan > 7)
	return 0;
    
    /* Make sure we are running at 16Mhz */
    clkf = SimGetFSys();
    SimSetFSys(16000000L);
    
    p = set_burst_params(bipolar ? ADBipolar[chan] : ADUnipolar[chan], period);

    /* Initialize global buffer parameters */
    _adbufp   = &databuf[0];
    _adbuflen = n & ~8;

    InstallHandler(ad_int_handler, QSPI_INT_VECTOR, 0);
    burst_start();
    while(_adbuflen > 0)
	Stop(0x2000);	/* wait for next interrupt */
    burst_stop();

    /*
    ** MAX186 A/D returns the 12-bit value left-shifted by 3.  The
    ** MSB is always zero.
    */
    if(bipolar)
    {
	/* Need to move the sign bit into the MSB before shifting right */
	while(n--)
	    databuf[n] = (databuf[n] << 1) >> 4;
    }
    else
    {
	while(n--)
	    databuf[n] >>= 3;
    }
    
    SimSetFSys(clkf);
    
    return p;
}
