/*
** $Id: events.h,v 4a9de31628cd 2007/04/20 21:53:14 mikek $
*/
#ifndef _EVENTS_H_
#define _EVENTS_H_

struct event {
    unsigned long	secs;
    unsigned short	msecs;
    unsigned short	len;
    void		*data;
    struct event	*next;
    char		name[1];
};

typedef struct _event_table {
    unsigned		ncells;
    struct event	*last;
    struct event	*e[1];
} EventTable;

void remove_event(EventTable *etp, const char *name);
int record_event(EventTable *etp, const char *name, time_tt t, void *what, 
		 unsigned short len);
int record_this(EventTable *etp, const char *name, void *what, 
		unsigned short len);
time_tt find_event(EventTable *etp, const char *name, void **whatp,
		   unsigned short *lenp);
double msec_diff(EventTable *etp, const char *name);
int event_exists(EventTable *etp, const char *name);
EventTable *new_event_table(unsigned size);
void delete_event_table(EventTable *etp);
void foreach_event(EventTable *etp, void (*func)(struct event*));

#endif
