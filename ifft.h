/**@file
** $Id: ifft.h,v 4a9de31628cd 2007/04/20 21:53:14 mikek $
*/
#ifndef _IFFT_H_
#define _IFFT_H_

#define FFT_FORWARD	0
#define FFT_INVERSE	1

double hamming_window(unsigned short *h, short n);
double hanning_window(unsigned short *h, short n);
void make_coeff(short *w, short n, int dir);
void ifft(long *xy, short ex, short *w);


#endif
