#!/usr/bin/env python
#
# Scons script to support cross-compiling for MLF2
#

PREFIX = 'm68k-coff-'

def generate(env):
    env['CC'] = PREFIX + 'gcc'
    env['AS'] = PREFIX + 'as'
    env['AR'] = PREFIX + 'ar'
    env['LD'] = PREFIX + 'ld'
    env['RANLIB'] = PREFIX + 'ranlib'
    env['CXX'] = PREFIX + 'g++'
    env['OBJCOPY'] = PREFIX + 'objcopy'
    env['PROGSUFFIX']   = '.coff'
    env['CCFLAGS'] = '-Os -mcpu32 -mshort -fshort-enums -fno-builtin -D__TT8__ -D__PICODOS__'
    env['LINKFLAGS'] = '$CCFLAGS -T $LINKFILE'
    env['LINK'] = '$CC'
    env['LIBPREFIX'] = 'lib'
    env['LIBSUFFIX'] = '.a'
    mkbin = env.Builder(action='$OBJCOPY -O binary -S $SOURCE $TARGET',
                        suffix='.run',
                        src_suffix='.coff',
                        single_source=True)
    mkhex = env.Builder(action='$OBJCOPY -O srec -S $SOURCE $TARGET',
                        suffix='.rhx',
                        src_suffix='.coff',
                        single_source=True)
    mkhex2 = env.Builder(action='$OBJCOPY -O srec -S $SOURCE $TARGET',
                         suffix='.ahx',
                         src_suffix='.coff',
                         single_source=True)
    env['BUILDERS']['PicoDos'] = mkbin
    env['BUILDERS']['Rhx'] = mkhex
    env['BUILDERS']['Ahx'] = mkhex2

def exists(env):
    return True
