/*
** $Id: events.c,v 4a9de31628cd 2007/04/20 21:53:14 mikek $
**
** EventTable object functions.  An EventTable associates a timestamp
** with a name (the key) and an optional pointer to an arbitrary block
** of data.  Timestamps are stored as seconds and milliseconds since
** 1/1/1970 00:00:00 (Unix and ANSI C epoch).
**
** TODO: this should be merged with the HashTable code (see hash.c)
*/
#include <stdlib.h>
#include <string.h>
#include <stddef.h>
#include <time.h>
#include <tt8lib.h>

#include "events.h"


static struct event*
alloc_node(size_t namelen, short datalen)
{
    register struct event	*ev;
    
    namelen = (namelen + 3) & ~3;
    if((ev = (struct event*)malloc(sizeof(struct event) + namelen + datalen)))
	ev->data = (char*)ev + sizeof(struct event) + namelen;
    return ev;
}

static int
hash(long base, register const char *name)
{
    unsigned long	sum = 0L;
    
    while(*name)
    {
	sum = (sum << 3) | (sum >> 29);
	sum ^= *name++;
    }
    
    return (sum % base);
}

static struct event*
lookup(EventTable *etp, const char *name)
{
    register struct event	*ev;

    /*
    ** The most recent lookup is cached in EventTable.last.  This
    ** allows calling event_exists() followed by find_event() on
    ** the same key without the performance hit of a second lookup.
    */
    if(etp->last && strcmp(name, etp->last->name) == 0)
	return etp->last;
    
    for(ev = etp->e[hash((long)etp->ncells, name)];ev != NULL;ev = ev->next)
	if(strcmp(name, ev->name) == 0)
	    break;
    return (etp->last = ev);
}

/**
 * new_event_table - Create a new EventTable.
 * @size: number of table entries
 *
 * Create a new EventTable data structure.  An EventTable is simply a
 * hash table with a timestamp associated with each entry.  The timestamp
 * is stored as seconds and milliseconds since 1/1/1970 (Unix time).  The
 * return value is a pointer to a new EventTable or NULL if memory allocation 
 * fails.
 */
EventTable*
new_event_table(unsigned size)
{
    register EventTable	*etp;
    ulong		nbytes;
    
    nbytes = sizeof(EventTable) + (size-1)*sizeof(struct event*);
    
    if((etp = malloc(nbytes)) != NULL)
    {
	etp->ncells = size;
	etp->last = 0;
	memset(etp->e, 0, size*sizeof(struct event*));
    }
    
    return etp;
}

/**
 * destroy_event_table - Free all memory associated with an EventTable.
 * @etp: pointer to EventTable.
 *
 * Free all memory associated with an EventTable.
 */
void
destroy_event_table(EventTable *etp)
{
    register unsigned		i = 0;
    register struct event	*p, *q;
    
    for(i = 0;i < etp->ncells;i++)
    {
	if(etp->e[i])
	{
	    /*
	    ** Free the chain rooted at this index
	    */
	    p = etp->e[i];
	    q = 0;
	    while(p)
	    {
		q = p->next;
		free(p);
		p = q;
	    }
	}
    }
    
    free(etp);
}


/**
 * record_event - Record an event by associating a timestamp with a name.
 * @etp: pointer to &EventTable
 * @name: name of the event.
 * @t: timestamp (seconds and "ticks").
 * @what: opaque data handle to associate with the event.
 * @len: size (in bytes) of the data.
 *
 * This function records an event by associating a timestamp with a 
 * name.  An arbitrary block of data, @what, may also be associated with the
 * name.  If @len is greater than zero, the data is copied into the event
 * table.  Returns 1 if sucessful, 0 if event could not be stored.
 */
int
record_event(EventTable *etp, const char *name, time_tt t, void *what, 
	     unsigned short len)
{
    struct event	*ev;
    int			hval;
    
    if((ev = lookup(etp, name)) == NULL)
    {
	if((ev = alloc_node(strlen(name), len)) == NULL)
	    return 0;
	strcpy(ev->name, name);
	hval = hash((long)etp->ncells, name);
	ev->next = etp->e[hval];
	etp->e[hval] = ev;
    }

    ev->secs  = t.secs;
    ev->msecs = (t.ticks * 1000L)/GetTickRate();
    if(len > 0)
	memcpy(ev->data, what, (size_t)len);
    else
	ev->data = what;

    ev->len = len;    
    return 1;
}


/**
 * record_this - Record an event using the current time as the timestamp.
 * @etp: pointer to &EventTable
 * @name: name of the event.
 * @what: opaque data handle to associate with the event.
 * @len: size (in bytes) of the data.
 *
 * Calls record_event() to store an event using the current time as the
 * timestamp.
 */
int 
record_this(EventTable *etp, const char *name, void *what, unsigned short len)
{
#ifdef __GNUC__
    return record_event(etp, name, ttm_now(), what, len);
#else
    return record_event(etp, name, ttmnow(), what, len);
#endif
}


/**
 * remove_event - Remove a named event from the table.
 * @etp: pointer to EventTable.
 * @name: event name.
 *
 * Remove the event associated with @name from the table.
 */
void
remove_event(register EventTable *etp, const char *name)
{
    register struct event	*p, *q;
    int				hval = hash((long)etp->ncells, name);
    
    q = 0;
    for(p = etp->e[hval];p != NULL;q = p,p = p->next)
	if(strcmp(name, p->name) == 0)
	{
	    if(q == 0)
		etp->e[hval] = p->next;
	    else
		q->next = p->next;
	    free(p);
	    break;
	}
}

/**
 * event_exists - Determine whether a named event has been recorded.
 * @etp: pointer to EventTable.
 * @name: name of the event.
 *
 * Check the table for an event associated with @name.  Returns true if the 
 * event exists otherwise false.
 */
int
event_exists(EventTable *etp, const char *name)
{
    return (lookup(etp, name) != NULL);
}

/**
 * find_event - Return the timestamp for an event.
 * @etp: pointer to EventTable.
 * @name: name of the event.
 * @whatp: returned data handle (if non-NULL)
 * @lenp: will contain the data size on return (if non-NULL).
 *
 * Find the event associated with @name and return it's timestamp along
 * with the data handle.  Returns a timestamp of (0,0) if the event
 * cannot be found.
 */
time_tt
find_event(EventTable *etp, const char *name, void **whatp, 
	   unsigned short *lenp)
{
    time_tt		retval = {0L, 0L};
    struct event	*ev;
    
    if((ev = lookup(etp, name)) != NULL)
    {
	retval.secs = ev->secs;
	retval.ticks = (ev->msecs * GetTickRate())/1000L;
	if(whatp)
	{
	    if(ev->len > 0)
		memcpy(*whatp, ev->data, (size_t)ev->len);
	    else
		*whatp = ev->data;
	}
	
	if(lenp)
	    *lenp = ev->len;
    }
    
    return retval;
}

/**
 * foreach_event - Call a supplied function for each entry in the event table.
 * @etp: pointer to EventTable.
 * @func: function to apply to each entry.
 *
 * Walk the event table and call @func for each element.
 */
void
foreach_event(EventTable *etp, void (*func)(struct event*))
{
    register unsigned		i = 0;
    register struct event	*ev;
    
    for(i = 0;i < etp->ncells;i++)
    {
	if((ev = etp->e[i]))
	    while(ev)
	    {
		(*func)(ev);
		ev = ev->next;
	    }
    }
}

/**
 * msec_diff - Calculate the elapsed time since the event was recorded.
 * @etp: pointer to EventTable.
 * @name: name of the event.
 *
 * Return the elapsed time in milliseconds since the event associated
 * with @name was recorded or -1.0 if the event was not found.
 */
double
msec_diff(EventTable *etp, const char *name)
{
    double		retval = -1.0;
    time_tt		now;
    long		dsecs, dmsecs;
    struct event	*ev;
    
    if((ev = lookup(etp, name)) != NULL)
    {
#ifdef __GNUC__
	now = ttm_now();
#else
	now = ttmnow();
#endif

	dsecs = now.secs - ev->secs;
	dmsecs = ((now.ticks * 1000L)/GetTickRate()) - ev->msecs;
	
	retval = dsecs*1000. + dmsecs;
    }

    return retval;
}

