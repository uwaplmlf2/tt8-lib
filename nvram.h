/**@file
** $Id: nvram.h,v 4a9de31628cd 2007/04/20 21:53:14 mikek $
*/
#ifndef _NVRAM_H_
#define _NVRAM_H_

//** Maximum key length */
#define NV_KEYLEN	16L

/** Entry value type codes */
#define NV_CHARS	0
#define NV_INT		1
#define NV_FLOAT	2

/**
 * Value stored in NVRAM table.
 */
typedef union {
    unsigned char	c[8]; /**< character string */
    long		l; /**< long integer */
    double		d; /**< double precision float */
} nv_value;

/**
 * Entry from NVRAM table.
 */
typedef struct {
    char		key[NV_KEYLEN]; /**< entry key */
    nv_value		value; /**< value */
    unsigned char	type; /**< type code */
    unsigned char	link; /**< link to next entry with same hash code */
} nv_entry;

/** Callback function passed to nv_foreach() */
typedef void (*nv_callback)(nv_entry*, void*);

/** Convenience macro */
#define init_nvram()	nv_init(NULL, uee_read, uee_write)

int nv_size(void);
void nv_foreach(nv_callback f, void *calldata);
void nv_init(int (*initf)(void), int (*readf)(char*,int), 
	     int (*writef)(char*,int));
int uee_read(char *dst, int n);
int uee_write(char *src, int n);
int nv_write(void);
int nv_insert(const char *name, nv_value *value, int type, int dowrite);
int nv_locate(const char *name);
int nv_lookup(const char *name, nv_value *value);
int nv_index_lookup(int idx, nv_value *value);
void nv_delete(const char *name);
void nv_show_all(void);

#endif
