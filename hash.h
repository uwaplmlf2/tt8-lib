/**@file
** $Id: hash.h,v 4a9de31628cd 2007/04/20 21:53:14 mikek $
*/
#ifndef _HASH_H_
#define _HASH_H_

/*
** Constants which define whether the hash table keys are case sensitive
** (standard) or case insensitive.
*/
#define HT_STANDARD	0
#define HT_NOCASE	1

typedef int (*ht_hash)(long base, const char *name);
typedef int (*ht_cmp)(const char *s1, const char *s2);

struct elem {
    unsigned short	len;
    void		*data;
    struct elem		*next;
    char		name[1];
};

typedef struct _hash_table {
    unsigned		ncells;
    ht_hash		func;
    ht_cmp		cmp;
    struct elem		*last;
    struct elem		*e[1];
} HashTable;

typedef void (*ht_callback)(struct elem*, void*);

void ht_remove_elem(HashTable *htp, const char *name);
int ht_insert_elem(HashTable *htp, const char *name, const void *what, 
		 unsigned short len);
int ht_find_elem(HashTable *htp, const char *name, void **whatp,
		   unsigned short *lenp);
int ht_elem_exists(HashTable *htp, const char *name);
HashTable *ht_create(unsigned size, int type);
void ht_destroy(HashTable *htp);
void ht_foreach_elem(HashTable *htp, ht_callback func, void *calldata);

#endif /* _HASH_H */
