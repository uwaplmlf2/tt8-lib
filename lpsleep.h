/**@file
** $Id: lpsleep.h,v 4a9de31628cd 2007/04/20 21:53:14 mikek $
*/
#ifndef _LPSLEEP_H_
#define _LPSLEEP_H_

#define LP_TOOSHORT	(-1)
#define LP_IRQBUSY	(-2)

typedef void (*sleephook)(void);

void protect_tpu_pin(unsigned short pnum);
void unprotect_tpu_pin(unsigned short pnum);
int add_before_hook(const char *name, sleephook h);
void remove_before_hook(const char *name);
int add_after_hook(const char *name, sleephook h);
void remove_after_hook(const char *name);
void init_sleep_hooks(void);
int lp_sleep_till(time_tt wakeup, short allow_serial);
int isleep_till(time_tt wake);
int isleep(long secs);
int copy_lp_function(void);

#endif
