/**@file
** $Id: menu.h,v 4a9de31628cd 2007/04/20 21:53:14 mikek $
*/
#ifndef _MENU_H_
#define _MENU_H_

/** maximum menu length */
#define MAX_MENU_LINES	18


typedef enum {MENTRY_FUNC=0, MENTRY_MENU} mentry_type;
typedef enum {MENU_PERM=0, MENU_POPUP} menu_type;

typedef void (*MenuCallback)(void*);

/**
 * Menu entry data structure.
 * Each entry represents either a function (callback) or
 * a pointer to a submenu.
 */
typedef struct _mentry {
    const char		*label; /**< label string */
    mentry_type		type;   /**< MENTRY_FUNC or MENTRY_MENU */
    void		*action; /**< function to call when selected */
    void		*call_data; /**< data passed to call-back function */
} MenuEntry;

/**
 * Menu data structure.
 * Contains one or more entries. A menu can be either a pop-up type,
 * where the selection of an entry closes the menu, or a standard
 * (permanent) type, where the menu is closed by selecting "quit".
 */
typedef struct _menu {
    const char		*title; /**< title string */
    menu_type		type; /**< MENU_PERM or MENU_POPUP */
    int			nr_entries; /**< number of entries */
    MenuEntry		*entries; /**< array of menu entries */
} Menu;


int show_menu(Menu *menu, const char *from);

#endif

