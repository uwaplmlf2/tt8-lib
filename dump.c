/*
** $Id: dump.c,v 4a9de31628cd 2007/04/20 21:53:14 mikek $
**
** Module to allow dumping the running application to a file such that
** it can be restarted from its current point when PicoDOS loads the
** file.
**
*/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <setjmp.h>
#include <tt8.h>
#include <tt8lib.h>
#include "util.h"

#define STACK_START	0x200000L
#define STACK_SIZE	0x4000L
#define TEXT_START	(STACK_START+STACK_SIZE)

/* heap pointers */
extern void* 	_mtop;
extern void* 	_mbot;
extern void* 	_mcur;

static void undump(void);

static jmp_buf state;

static void
undump_c(void)
{
    longjmp(state, 1L);
}

/*
 * This function will execute when the program restarts.  It copies the
 * saved stack from the top of the heap and calls undump_c().
 */
asm("
    .text
    .align 4
undump:
    movel #0x4000,%d0
    lsrl  #2,%d0
    movel _mcur,%a0
    moveal #0x200000,%a1
L1: movel %a0@+,%a1@+
    subql #1,%d0
    bne   L1

    movel _mtop,%sp
    jmp   undump_c
");

/**
 * dump - dump the image of a running program to a file.
 * @filename: name of file (must end in '.run')
 *
 * This function dumps the current state of the running program to the named
 * file which will be loadable by PicoDOS.  When the dump-file is loaded it
 * will restart at the current point (i.e. the restarted program will return
 * from dump).  There are three possible return values (the constants are
 * defined in util.h):
 *
 *	%DUMP_ERROR     -  the dump procedure failed.
 *
 *      %DUMP_OK        -  the dump procedure succeeded.
 *
 *      %DUMP_RESTART   -  the program has been restarted.
 */
int
dump(const char *filename)
{
    int			fd, i;
    unsigned long	nr_bytes, nr_blocks;
    char		*p;
    
    if(setjmp(state) != 0)
	return DUMP_RESTART;
    
    if((fd = open(filename, O_WRONLY|O_BINARY)) < 0)
	return DUMP_ERROR;

    if((unsigned long)(_mcur + STACK_SIZE) > (unsigned long)_mtop)
	return DUMP_ERROR;
    
    memcpy(_mcur, (void*)STACK_START, STACK_SIZE);
    
    /* Force the restarted program to jump to undump at startup */
    *((unsigned long*)TEXT_START+2L) = (unsigned long)undump;

    PutStr("Dumping program image ");
    
    /* Dump the program image to a file */
    p = (char*)TEXT_START;
    nr_bytes = (unsigned long)(_mcur + STACK_SIZE) - TEXT_START;
    nr_blocks = nr_bytes >> 10;
    nr_bytes = nr_bytes & 1023;
    for(i = 0;i < nr_blocks;i++)
    {
	SerPutByte('.');
	if(write(fd, p, 1024L) != 1024L)
	{
	    PutStr("failed!\n");
	    close(fd);
	    return DUMP_ERROR;
	}
	
	p += 1024L;
    }

    if(nr_bytes)
    {
	SerPutByte('.');
	if(write(fd, p, nr_bytes) != nr_bytes)
	{
	    PutStr("failed!\n");
	    close(fd);
	    return DUMP_ERROR;
	}
    }
    
    PutStr(" done\n");
    
    close(fd);
    return DUMP_OK;
}

    
