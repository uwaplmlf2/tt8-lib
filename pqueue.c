/**@file
** $Id: pqueue.c,v 4a9de31628cd 2007/04/20 21:53:14 mikek $
**
** Binary heap implementation of a priority queue.
**
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stddef.h>
#include <time.h>
#include "pqueue.h"

#define PRIO_CMP(p, i, j)	(p)->cmp((p)->e[(i)], (p)->e[(j)])
#define PRIO_LT(p, i, j) 	(PRIO_CMP(p, i, j) == -1)
#define PRIO_LE(p, i, j) 	(PRIO_CMP(p, i, j) != 1)
#define PRIO_EQ(p, i, j) 	(PRIO_CMP(p, i, j) == 0)
#define PRIO_GE(p, i, j) 	(PRIO_CMP(p, i, j) != -1)
#define PRIO_GT(p, i, j) 	(PRIO_CMP(p, i, j) == 1)

static int
icmp(void *n1, void *n2)
{
    return (n1 == n2 ? 0 : (n1 < n2 ? -1 : 1));
}

static PQueue*
pq_malloc(long size)
{
    long	nbytes = sizeof(PQueue) + size*sizeof(void*);
    return (PQueue*)malloc(nbytes);
}

/**
 * Create a new priority queue object.
 * Create and initialize a new priority queue object.  f is the comparison
 * function to use when comparing nodes.  The nodes are stored as opaque
 * pointers (void*) and the queue knows nothing about the contents.  The
 * comparison function is called as follows.  
 * @verbatim
 *	r = (*f)(n1, n2)
 * @endverbatim
 * Where n1 and n2 are void*.  The function should return one of the following
 * values:
 * - 1 if priority(n1) > priority(n2)
 * - 0 if priority(n1) == priority(n2)
 * - -1 if priority(n1) < priority(n2)
 *
 * If f is NULL, the elements will be compared numerically as if they were
 * long integers.

 * @param  size  maximum queue size
 * @param  f  comparison function.
 * @return a pointer to the new object or NULL if there is insufficient memory.
 */

PQueue* 
pq_create(long size, fcmp f)
{
    PQueue	*pq;
    
    if((pq = pq_malloc(size)) == NULL)
	return NULL;
    
    pq->size = size;
    pq->n = 0;
    pq->cmp = f != NULL ? f : icmp;
    
    return pq;
}

/**
 * Free the memory associated with a priority queue.
 *
 * @param  pq  pointer to PQueue.
 */
void
pq_destroy(PQueue *pq)
{
    free(pq);
}

/**
 * Add a new element to the priority queue.
 *
 * @param  pq  pointer to PQueue.
 * @param  e  new element.
 * @return 1 if successful or 0 if the queue is full.
 */
int
pq_insert(PQueue *pq, void *e)
{
    long	m;
    void	*t;

    if(pq->n == pq->size)
	return 0;
    
    pq->n++;
    m = pq->n;
    while(m > 1 && (pq->cmp(e, pq->e[m/2]) == 1))
    {
	pq->e[m] = pq->e[m/2];
	m = m/2;
    }

    pq->e[m] = e;
    
    return 1;
}

static void
pq_dump(PQueue *pq, FILE *fp, long mark)
{
    long	i;
    
    for(i = 1;i <= pq->n;i++)
    {
	if(mark == i)
	    fputc('*', fp);
	fprintf(fp, "%ld,", pq->e[i]);
    }
    fputc('\n', fp);
}

static void
reorder(PQueue *pq)
{
    long	u, v, w;
    void	*e;

    /*
    ** Reorder the tree after the root has been removed.  The last element
    ** in the tree (rightmost leaf) is saved and then we walk down the tree
    ** and promote the highest priority child to its parent node.
    */
    e = pq->e[pq->n+1];
    u = 1;		/* parent */
    v = 2*u;		/* left child */
    while(v <= pq->n)
    {
	w = v + 1;	/* right child */

	/*
	** Find the highest priority child and compare with the saved node.
	** If the child has lower priority, we're done.
	*/
	if(w <= pq->n && pq->cmp(pq->e[v], pq->e[w]) == -1)
	    v = w;
	if(pq->cmp(pq->e[v], e) != 1)
	    break;

	pq->e[u] = pq->e[v];	/* copy highest priority child to parent */
	u = v;
	v = 2*u;
    }

    pq->e[u] = e;
}

/**
 * Remove the top item from the priority queue.
 *
 * @param  pq  pointer to PQueue.
 * @return the element or NULL if the queue is empty.
 */
void*
pq_remove(PQueue *pq)
{
    void	*t;
    
    if(pq->n == 0)
	return NULL;
    
    t = pq->e[1];
    pq->n--;
    if(pq->n > 0)
	reorder(pq);
    return t;
}

/**
 * Return the top item from the priority queue without removing it.
 *
 * @param  pq  pointer to PQueue.
 * @return element or NULL if queue is empty.
 */
void*
pq_peek(PQueue *pq)
{
    return pq->n == 0 ? NULL : pq->e[1];
}

/**
 * Check whether priority queue is empty.
 *
 * @param  pq  pointer to PQueue.
 * @return true if queue is empty.
 */
#ifndef __GNUC__
int
pq_empty(PQueue *pq)
{
    return (pq->n == 0);
}
#endif

#ifdef PQ_TEST
#include <stdio.h>
#include <string.h>
#ifdef __TT8__
#include <tt8lib.h>
#endif

static int
file_comp(void *s1, void *s2)
{
    return strcmp(((char*)s1)+3, ((char*)s2)+3);
}

    
int
main(int ac, char *av[])
{
    PQueue	*pq;
    int		i;
    long	x;

#ifdef __TT8__
    InitTT8(NO_WATCHDOG, TT8_TPU);
    SimSetFSys(16000000L);
#endif
    

    if((pq = pq_create(100, file_comp)) == NULL)
    {
	printf("Cannot create queue\n");
	exit(-1);
    }
    
    pq_insert(pq, "env0001.nc");
    pq_insert(pq, "env0010.nc");
    pq_insert(pq, "env0006.nc");
    pq_insert(pq, "fpr0003.nc");
    pq_insert(pq, "fpr0001.nc");
    
    while(!pq_empty(pq))
	printf("%s\n", (char*)pq_remove(pq));
    
    pq_destroy(pq);
    
    return 0;
}

#endif
