/*
** $Id: deriv.c,v 4a9de31628cd 2007/04/20 21:53:14 mikek $
**
** Calculate derivatives using the z-transform.
**
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "deriv.h"


static double
dot(double *x, double *y, int n)
{
    double	sum = 0.;
    
    while(n--)
    {
	sum += (*x)*(*y);
	x++;
	y++;
    }
    
    return sum;
}

static double
real_update(zDiff *zdp, double y)
{
    double	*p, *q;
    int		i;
    
    p = &zdp->ybuf[0];
    q = &zdp->ybuf[1];
    for(i = 0;i < zdp->size-1;i++)
	*p++ = *q++;
    *p = y;
    return dot(zdp->ybuf, zdp->C, zdp->size);
}

static double
pre_update(zDiff *zdp, double y)
{
    double	*p, *q;
    int		i;
    
    if(zdp->n == 1)
    {
	zdp->update = real_update;
	return real_update(zdp, y);
    }
    
    p = &zdp->ybuf[0];
    q = &zdp->ybuf[1];
    for(i = 0;i < zdp->size-1;i++)
	*p++ = *q++;
    *p = y;
    zdp->n--;
    return 0.;
}


zDiff*
zdiff_new(int nr_coeff, double coeff[], double h)
{
    int		nbytes, i, j;
    zDiff	*zdp;
    
    nbytes = sizeof(zDiff) + 2*nr_coeff*sizeof(double);
    if((zdp = (zDiff*)malloc(nbytes)) == NULL)
	return NULL;

    zdp->C = (double*)((char*)zdp + sizeof(zDiff));
    zdp->ybuf = zdp->C + nr_coeff;
    
    zdp->n = zdp->size = nr_coeff;

    for(i = 0,j = nr_coeff-1;i < nr_coeff;i++,j--)
	zdp->C[i] = coeff[j]/h;

    memset(zdp->ybuf, 0, nr_coeff*sizeof(double));
    zdp->update = pre_update;
    
    return zdp;
}


void
zdiff_reset(zDiff *zdp)
{
    memset(zdp->ybuf, 0, zdp->size*sizeof(double));
    zdp->update = pre_update;
    zdp->n = zdp->size;
}
