/**@file
** arch-tag: 967774b6-9eb6-4ce1-af46-95a081b7e227
*/
#ifndef _QUEUE_H_
#define _QUEUE_H_


/** Queue entry (node) */
typedef struct qnode {
    struct qnode	*next; /**< pointer to next node */
    struct qnode	*prev; /**< pointer to previous node */
    void		*data; /**< opaque data pointer */
} QNode;

/** Queue data structure */
typedef struct tqueue {
    QNode		*head; /**< Queue head */
    QNode		*tail; /**< Queue tail */
    int			count; /**< Queue length */
} Queue;


/**
 * Initialize a Queue.
 *
 * @param  q  pointer to Queue data structure.
 */
static __inline__ void queue_init(Queue *q)
{
    q->head = q->tail = NULL;
    q->count = 0;
}

/**
 * Destroy a queue (this is a no-op).
 *
 * @param  q  pointer to Queue data structure.
 */
static __inline__ void queue_destroy(Queue *q)
{
}

/**
 * Queue length.
 *
 * @param  q  pointer to Queue data structure.
 * @return queue length.
 */
static __inline__ int queue_size(Queue *q)
{
    return q->count;
}

/**
 * Return (but do not remove) the next element from the queue.
 *
 * @param  q  pointer to Queue data structure.
 * @return pointer to next element or NULL if Queue is empty.
 */
static __inline__ QNode* queue_peek(Queue *q)
{
    return q->tail;
}

typedef void (*q_func)(QNode *np, void *datap);

int queue_put(Queue *q, QNode *np);
int queue_put_tail(Queue *q, QNode *np);
QNode *queue_get(Queue *q);
void queue_walk(Queue *q, q_func f_node, void *datap);


#endif /* _QUEUE_H_ */
