/*
** $Id: eidi.h,v 4a9de31628cd 2007/04/20 21:53:14 mikek $
**
** Macros to disable and restore interrupts.
**
*/
#ifndef _EIDI_H_
#define _EIDI_H_

#ifdef __GNUC__

/* Disable interrupts and return status register */
#define idisable()\
  ({ ushort __sr;\
     __asm__ __volatile__("move.w %%sr,%0\n"\
                          "ori.w #0x700,%%sr"\
                           : "=d" (__sr));\
     __sr;})

/* Restore status register */
#define irestore(x)\
  ({ ushort __sr = (x);\
     __asm__ __volatile__("move.w %0,%%sr" :: "g" (__sr));})

#define cli(x)	(x = idisable())
#define sti(x)	(irestore(x))
#else	/* AZTEC C */

#define cli(x)	do {\
                       (x) = GetStatusReg();\
                       SetStatusReg((x) | 0x700);\
                   } while(0)
#define sti(x)	SetStatusReg(x)

#endif

#endif /* _EIDI_H_ */
