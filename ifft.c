/**@file
** $Id: ifft.c,v 4a9de31628cd 2007/04/20 21:53:14 mikek $
**
** 1D complex-to-complex integer FFT functions.
**
*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "ifft.h"

#define ISWAP(a,b) 	do {long t = (a);(a) = (b);(b) = t;} while(0)

/**
 * Generate fixed-point Hamming Window coefficients.
 * Generate coefficients for an N-point Hamming window.  The coefficients are
 * stored as unsigned 16-bit integers -- the binary point is to the left of
 * bit 15.
 *
 * @param  h  coefficient array
 * @param  n  dimension of @h
 * @returns the sum of the squares of the window coefficients for later scaling of the spectrum.  
 */
double
hamming_window(unsigned short *h, short n)
{
    register int	i;
    double		twopi = atan2(1., 0.)*4.0, w, sumsq;
    double		scale = twopi/(double)(n - 1);
    
    /*
    ** h[i] = 65535*(0.54 - 0.46*cos(2*pi*i/(n-1)))
    */
    sumsq = 0.;
    for(i = 0;i < n;i++)
    {
	w = 0.54 - 0.46*cos(scale*i);
	sumsq += (w*w);
	h[i] = (unsigned short)(65535.*w);
    }
    
    return sumsq;
}

/**
 * Generate fixed-point Hanning Window coefficients.
 * Generate coefficients for an N-point Hanning window.  The coefficients are
 * stored as unsigned 16-bit integers -- the binary point is to the left of
 * bit 15.
 *
 * @param  h  coefficient array
 * @param  n  dimension of @h
 * @returns the sum of the squares of the window coefficients for later scaling of the spectrum.  
 */
double
hanning_window(unsigned short *h, short n)
{
    register int	i;
    double		w, sumsq;
    double		scale = 2*acos(-1.)/n;
    
    sumsq = 0.;
    for(i = 0;i < n;i++)
    {
	w = (1. - cos(scale*i))/2.;
	sumsq += (w*w);
	h[i] = (unsigned short)(65535.*w);
    }
    
    return sumsq;
}

/**
 * Generate the complex coefficients for a fixed-point FFT
 * Generate the complex coefficients for an N-point FFT.  Upon return
 * w will contain n/2 complex values. The real components will be stored 
 * in the even indicies and the imaginary components in the odd. dir must
 * be one of the following constants (defined in ifft.h):
 * - FFT_FORWARD
 * - FFT_INVERSE
 *
 * @param  w  coefficient array.
 * @param  n  dimension of w.
 * @param  dir  direction constant.
 */
void
make_coeff(register short *w, short n, int dir)
{
    register int	i;
    double		pi = atan2(1., 0.)*2;
    double		arg, nd, sc, cc;
    
    nd = (double)n;
    
    cc = (1L << 15) - 1.;
    sc = (dir == FFT_FORWARD) ? -cc : cc;
    
    for(i = 0;i < n;i+=2)
    {
	arg = pi*i/nd;
	w[i]   = (short)(cc * cos(arg));
	w[i+1] = (short)(sc * sin(arg));
    }
}


/**
 * 1-D radix-2 complex-to-complex integer FFT.
 * Perform an in-place 1-D complex-to-complex integer FFT on the values
 * in xy using the coefficients w created by make_coeff().  The 
 * coefficients determine the direction of the transform.
 *
 * Overflow Note:  overflow will occur if the following relation is
 * true where MAX is the maximum input value.
 *
 * \f$MAX*2^{(2*ex-2)} > 2^{31}\f$
 *
 * @param  xy  array of \f$2^{ex}\f$ complex data values.
 * @param  ex  base-2 exponent of the dimension of the transform.
 * @param  w   array of \f$2^{(ex-1)}\f$ complex coefficients
 */
void 
ifft(long *xy, short ex, short *w)
{
    short 		n, n2, ie, ia, c, s, shift;
    register short 	i, j, k;
    register long	yt, xt, *top, *bot;

    n = (1 << ex);
    shift = 15 - ex;
    
    ie = 1;
    for (k=n; k > 1; k = (k >> 1) )
    {
	n2 = k>>1;
	ia = 0;
	for (j=0; j < n2; j++)
        {
	    /*
	    ** Pre-shift the coefficients to avoid overflow.
	    */
	    c = w[2*ia] >> shift;
	    s = w[(2*ia)+1] >> shift;

	    ia = ia + ie;
	    top = xy + 2*j;		/* Top element of butterfly */
	    bot = xy + 2*(j + n2);	/* Bottom element of butterfly */
	    
	    for (i=j; i < n; i += k)
            {
		xt      = (top[0] - bot[0]);
		top[0]  = (top[0] + bot[0]);
		yt      = (top[1] - bot[1]);
		top[1]  = (top[1] + bot[1]);

		bot[0]  = ((c*xt) - (s*yt)) >> ex;
		bot[1]  = ((c*yt) + (s*xt)) >> ex;
				
		top += (2*k);
		bot += (2*k);
            }
        }
        ie = ie<<1;
    }

    /* Bit reverse and swap */    
    n2 = n << 1;
    j = 0;
    for(i = 0;i < n2;i += 2)
    {
	if(j > i)
	{
	    ISWAP(xy[i], xy[j]);
	    ISWAP(xy[i+1], xy[j+1]);
	}
	k = n2 >> 1;
	while(k >= 2 && j >= k)
	{
	    j -= k;
	    k >>= 1;
	}
	j += k;
    }
}

#ifdef FFTTEST
#include <tt8.h>
#include <tt8lib.h>
#include <tpu332.h>
#include <qsm332.h>
#include <picodcf8.h>

#define FFTLEN_EX	10
#define FFTLEN		(1L << FFTLEN_EX)
#define DELTA_T		1.0e-5

static long 		itrans[2*FFTLEN];
static short 		data[FFTLEN];
static short		W[FFTLEN];

static unsigned short 	H[FFTLEN];
static float 		spec[FFTLEN+2];

int
main(int ac, char *av[])
{
    int		i, j;
    long	mean;
    float	re, im, w;
    float	scale = 2.*DELTA_T*1.0e-6;

    /*
    ** Hardware initialization
    */
    InitTT8(NO_WATCHDOG, TT8_TPU);
    InitCF8(CF8StdCS, CF8StdAddr);
    if(errno != 0)
	printf("\nWARNING: InitCF8 failed, error %d\n", errno);
    if(errno == -1 || errno == PiDosNoHardware)
	goto done;
    
    SimSetFSys(16000000L);
    
    for(i = 0;i < FFTLEN;i++)
	data[i] = i+1;
    
    w = hamming_window(H, (short)FFTLEN);
    make_coeff(W, (short)FFTLEN, FFT_FORWARD);

    printf("Window and remove mean\n");
    StopWatchStart();
    mean = 0L;
    for(i = 0;i < FFTLEN;i++)
	mean += data[i];
	
    mean /= FFTLEN;

    /*
    ** Remove the mean and apply a Hamming Window to the data before
    ** writing it to the transform array.
    */
    for(i = 0,j = 0;i < FFTLEN;i++)
    {
	data[i] -= mean;
	itrans[j++] = (H[i]*(long)data[i])/65536;
	itrans[j++] = 0L;
    }
    printf("Elapsed time = %lu usecs\n", StopWatchTime());
    
    printf("Starting FFT\n");
    
    /* Fixed-point FFT */
    StopWatchStart();
    ifft(itrans, FFTLEN_EX, W);
    printf("Elapsed time = %lu usecs\n", StopWatchTime());
    
#if 0
    scale = scale/w;
    
    for(i = 0;i <= FFTLEN+1;i+=2)
    {
	re = (float)itrans[i];
	im = (float)itrans[i+1];
	printf("%g\n", (re*re+im*im)*scale);
    }
#endif
    DelayMilliSecs(500L);

done:    
    return 0;
}

#endif
