/**@file
** arch-tag: 53ec1d31-b450-4d5e-95e1-3d15915b4aa5
** Time-stamp: <2007-06-22 11:21:09 mike>
**
** Simple, lightweight FIFO queue implementation.
**
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "queue.h"


/**
 * Insert a new node at the head of the queue.
 * Insert np at the head of the queue.  The node is not copied, it is up to
 * the caller to manage the memory.
 *
 * @param  q  pointer to Queue data structure.
 * @param  np  pointer to new node.
 * @return 0
 */
int
queue_put(Queue *q, QNode *np)
{
    np->prev = NULL;

    np->next = q->head;
    if(q->head)
        q->head->prev = np;
    q->head = np;
    q->count++;
    if(q->tail == NULL)
        q->tail = q->head;

    return 0;
}

/**
 * Insert a new item at the tail of the queue.
 * Insert np at the tail of the queue.  The node is not copied, it is up to
 * the caller to manage the memory.
 *
 * @param  q  pointer to Queue data structure.
 * @param  np  pointer to new node.
 * @return 0
 */
int
queue_put_tail(Queue *q, QNode *np)
{
    np->next = NULL;
    np->prev = q->tail;
    if(q->tail)
        q->tail->next = np;
    q->tail = np;
    q->count++;
    if(q->head == NULL)
        q->head = q->tail;

    return 0;
}

/**
 * Remove the next item from the tail of the queue.
 *
 * @param  q  pointer to Queue data structure.
 * @return point to QNode or NULL if queue is empty.
 */
QNode*
queue_get(Queue *q)
{
    QNode       *np;

    if(q->tail == NULL)
        return NULL;
    np = q->tail;
    q->tail = np->prev;
    if(q->tail)
        q->tail->next = NULL;
    q->count--;

    if(q->count == 0)
        q->head = NULL;
    np->prev = np->next = NULL;

    return np;
}

/**
 * Walk the queue and visit each node.
 *
 * @param  q  pointer to Queue data structure.
 * @param  f_node  function to process each node.
 * @param  datap  opaque data pointer passed to f_node
 */
void
queue_walk(Queue *q, q_func f_node, void *datap)
{
    QNode       *np;

    np = q->tail;
    while(np)
    {
        f_node(np, datap);
        np = np->prev;
    }
}
