/*
**$Id: picalarm.h,v 4a9de31628cd 2007/04/20 21:53:14 mikek $
*/
#ifndef _PICALARM_H_
#define _PICALARM_H_


#define PIC_ALARM_ONESHOT	1
#define PIC_ALARM_REPEAT	0

int pic_alarm_set(long secs, int type, void (*action)(void));
void pic_alarm_reset(void);
void pic_alarm_shutdown(void);

#endif
