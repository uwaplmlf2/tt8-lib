/**@file
** $Id: draw.c,v 4a9de31628cd 2007/04/20 21:53:14 mikek $
**
** Functions for drawing ASCII graphics on the console using
** ANSI escape codes.
**
*/
#include <stdio.h>
#include <stdlib.h>
#include "draw.h"

/** Escape code to place cursor at home position */
#define MOVE_HOME	"\033[1;1H"

/** Escape code to clear the screen */
#define CLEARSCR	"\033[2J"

/**
 * Clear the entire screen.
 * Clears entire screen and leaves cursor in upper left corner (home
 * position).
 */
void
drw_clear(void)
{
    fputs(MOVE_HOME, stdout);
    fputs(CLEARSCR, stdout);
    fflush(stdout);
}

/**
 * Move to specified row and column and write a character.
 * Move cursor to designated position and write c (if c > 0).
 *
 * @param  row  row number.
 * @param  col  column number.
 * @param  c  character to write.
 */
void
drw_moveto(int row, int col, int c)
{
    if(row <= 0 || col <= 0)
	return;
    printf("\033[%d;%dH", row, col);
    if(c > 0)
	fputc(c, stdout);
    
    fflush(stdout);
}

/**
 * Set background color.
 * Set the background color using one of the following color
 * codes defined in draw.h
 * - COLOR_BLACK
 * - COLOR_RED
 * - COLOR_GREEN
 * - COLOR_ORANGE
 * - COLOR_BLUE
 * - COLOR_YELLOW
 * - COLOR_CYAN
 * - COLOR_WHITE
 *
 * @param  color  color code.
 */
void
drw_setbg(drw_color color)
{
    printf("\033[4%dm", (int)color);
    fflush(stdout);
}


/**
 * Set foreground color.
 * Set the foreground color using one of the following color
 * codes defined in draw.h
 * - COLOR_BLACK
 * - COLOR_RED
 * - COLOR_GREEN
 * - COLOR_ORANGE
 * - COLOR_BLUE
 * - COLOR_YELLOW
 * - COLOR_CYAN
 * - COLOR_WHITE
 *
 * @param  color  color code.
 */
void
drw_setfg(drw_color color)
{
    printf("\033[3%dm", (int)color);
    fflush(stdout);
}


#ifdef TEST_DRAW
int
main(int ac, char *av[])
{
    int		r, c;
    
    drw_clear();
    fputs("100 -|", stdout);
    for(r = 2;r < 21;r++)
    {
	drw_moveto(r, 6);
	fputc('|', stdout);
    }
    
    drw_moveto(r, 1);
    fputs("  0 -|", stdout);
    for(c = 0;c < 52;c++)
	fputc('_', stdout);
    fputc('\n', stdout);
    
    fflush(stdout);
    return 0;
}
#endif /* TEST_DRAW */
