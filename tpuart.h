/**@file
** $Id: tpuart.h,v eb4504db00ef 2008/05/27 18:45:11 mikek $
*/
#ifndef _TPUART_H_
#define _TPUART_H_

#include <stddef.h>

/** Parity constants */
#define TPU_NO_PARITY		0
#define TPU_EVEN_PARITY		1
#define TPU_ODD_PARITY		2

/** TPUart.flags bit assignments */
#define TPU_ONLCR		0x01	/**< LF --> CRLF on output */
#define TPU_IGNCR		0x02	/**< CR ignored on input */
#define TPU_ICRNL		0x04	/**< CR --> LF on input */
#define TPU_NOBLOCK		0x08	/**< non-blocking input */

#define TPU_BUF_SIZE		2048L

/** TPUart data structure */
typedef struct {
    unsigned 		rchan :4;	/**< read channel */
    unsigned 		wchan :4;       /**< write channel */
    unsigned		flags :8;       /**< I/O flags */
    int			parity;         /**< serial parity */
    int			dbits;          /**< number of data bits */
    long 		baud;           /**< baud rate */
    unsigned char	rbuf[TPU_BUF_SIZE+TSER_MIN_MEM]; /**< input buffer */
    unsigned char	wbuf[TPU_BUF_SIZE+TSER_MIN_MEM]; /**< output buffer */
} TPUart;


TPUart* tpuart_open(int readpin, int writepin, long speed, int par, 
	    int dbits, int sbits, int flags);
void tpuart_set_flags(TPUart *t, int flags);

void tpuart_close(TPUart *t);
void tpuart_set_speed(TPUart *t, long baud);
size_t tpu_read(TPUart *t, char *buf, size_t n);
size_t tpu_write(TPUart *t, const char *buf, size_t n);
void tpu_pass_thru(TPUart *t, int stopchar, int breakchar, void (*hook)(void));
void tpuart_set_noblock(TPUart *t);
void tpuart_set_block(TPUart *t);
void tpuart_set_speed(TPUart *t, long baud);
void tpu_send_break(TPUart *t);
void tpuart_inflush(TPUart *t);

#endif
