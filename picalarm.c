/*
** $Id: picalarm.c,v 4a9de31628cd 2007/04/20 21:53:14 mikek $
**
** Simple alarm clock facility using the PIC.
**
*/
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <tt8lib.h>
#include <userio.h>
#include <tpu332.h>
#include <tt8.h>
#include <tat332.h>
#include <sim332.h>
#include <qsm332.h>
#include <dio332.h>
#include <tt8pic.h>
#include "eidi.h"
#include "picalarm.h"
#include "log.h"

#define CmpAlarmMask	0x82
#define PREnables	0x23

static void pic_handler(void);

static struct alarm_state {
    short	type;
    long	interval;
    void	(*action)(void);
} Alarm, *pAlarm = 0;

#define ACTIVATE	(pAlarm = &Alarm)
#define DEACTIVATE	(pAlarm = 0)

static void
pic_alarm_enable(long alarm_time)
{
    SetAlarmSecs(alarm_time);
    PConfInp(F, 3);
    PConfBus(F, 3);
    PicOrRF(PREnables, CmpAlarmMask);
}

/*
** Assembler part of the PIC ISR.  Passes the saved status register value
** to the C part of the handler.  Also takes care of saving and restoring
** the register set.
*/
#ifdef __GNUC__
asm(".text
     .align 4
pic_handler:
    movem.l   %a0-%a6/%d0-%d7,-(%sp)
    move.w    60(%sp),-(%sp)
    jsr       c_handler
    addq.l    #2,%sp
    movem.l   (%sp)+,%a0-%a6/%d0-%d7
    rte
");
#else /* AZTEC C */
#asm
    cseg
_pic_handler:
    movem.l    a0-a6/d0-d7,-(sp)
    move.w     60(sp),-(sp)
    jsr        _c_handler
    addq.l     #2,sp
    movem.l    (sp)+,a0-a6/d0-d7
    rte
#endasm
#endif

static void
c_handler(short sr)
{
    long	t = RtcToCtm();
    register struct alarm_state	*a = pAlarm;

    PConfInp(F, 3);
    PicAckCmpAlrm();
    PicAndRF(PREnables, ~CmpAlarmMask);
    if(Pin(F, 3) == 0)
    {
	SetAlarmSecs(0L);
	PicAckCmpAlrm();
    }
    
    if(a == 0)
	return;
    
    if(a->type == PIC_ALARM_ONESHOT)
    {
	DEACTIVATE;
    }
    else
	pic_alarm_enable(t + a->interval);

    SetStatusReg(sr);
    if(a->action)
	(a->action)();
}



/**
 * pic_alarm_set - set the PIC based alarm clock.
 * @secs: relative expiration time in seconds.
 * @type: alarm type
 * @action: handler function
 *
 * Start the PIC "alarm clock" and install a handler function to be called
 * when the alarm expires, @secs seconds from now.  @type must be one of
 * the following constants defined in picalarm.h:
 *
 * %PIC_ALARM_ONESHOT - @action is called once.
 *
 * %PIC_ALARM_REPEAT - @action is called every @secs seconds
 *
 * Returns 1 if successful, 0 if an alarm is already active.
 */
int
pic_alarm_set(long secs, int type, void (*action)(void))
{
    unsigned long	*vbr;
    
    /*
    ** Return if alarm is active.
    */
    if(pAlarm != 0)
    {
	log_error("picalarm", "Alarm is already set\n");
	return 0;
    }

    Alarm.interval = secs;
    Alarm.type     = type;
    Alarm.action   = action;

    vbr = (unsigned long*)GetVBR();
    vbr[Level_3_Interrupt] = (unsigned long)pic_handler;
    pic_alarm_enable(RtcToCtm() + secs);
    
    ACTIVATE;
    
    return 1;
}

/**
 * pic_alarm_shutdown - stop an alarm
 *
 * Shutdown a pending alarm.
 */
void
pic_alarm_shutdown(void)
{
    DEACTIVATE;
    SetAlarmSecs(0L);
}

/**
 * pic_alarm_reset - reset an alarm.
 *
 * Reset a pending alarm so that the countdown starts from the current time.
 */
void
pic_alarm_reset(void)
{
    short	state;
    register struct alarm_state	*a = pAlarm;
    
    if(a == 0)
	return;
    
    pic_alarm_shutdown();
    cli(state);
    pic_alarm_enable(RtcToCtm() + Alarm.interval);
    ACTIVATE;
    sti(state);
}

