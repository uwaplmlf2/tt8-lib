/*
** $Id: ppwa.h,v 4a9de31628cd 2007/04/20 21:53:14 mikek $
*/
#ifndef _PPWA_H_
#define _PPWA_H_

int ppwa_init(int chan, int nr_periods);
void ppwa_stop(void);
int ppwa_valid(void);
long ppwa_read(void);

#endif /* _PPWA_H_ */
