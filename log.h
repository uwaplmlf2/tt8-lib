/*
** $Id: log.h,v 4a9de31628cd 2007/04/20 21:53:14 mikek $
*/
#ifndef _LOG_H_
#define _LOG_H_

#ifndef __GNUC__
#define __attribute__(x)
#endif

int openlog(const char *fname);
int appendlog(const char *fname);
void closelog(void);
void log_error(const char *tag, const char *str, ...) 
    __attribute__ ((format(printf, 2, 3)));
void log_event(const char *str, ...) __attribute__ ((format(printf, 1, 2)));
int log_copy_errors(const char *file, long tail_seek);

#endif
