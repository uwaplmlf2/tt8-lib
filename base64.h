/*
** arch-tag: b02f138b-1963-445d-acbf-1d057ffb90fa
** $Id: base64.h,v ae830e65a65e 2007/10/10 21:11:00 mikek $
*/
#ifndef _BASE64_H_
#define _BASE64_H_

int b64_write(FILE *ofp, const unsigned char *data, int n);


#endif /* _BASE64_H_ */
