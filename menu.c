/**@file
** $Id: menu.c,v 4a9de31628cd 2007/04/20 21:53:14 mikek $
**
** Simple-minded menu display functions
**
*/
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#include <tpu332.h>
#include <tt8lib.h>
#include <ctype.h>
#include "menu.h"

#define ESC 		'\033'
#define BACKSPACE	'\010'
#define CANCEL		'\003'
#define DEL		'\177'

static void 
display_menu(const char *title, register MenuEntry *menu, size_t n,
  const char *last, int oneshot)
{
    register int	i;
    int			quit = n + 1, slen;

    slen = strlen(title);
    printf("\n\n    %s\n", title);
    for(i = 0;i < slen+8;i++)
	putchar('-');
    putchar('\n');

    for(i = 0;i < n;i++)
	printf("%d)  %s %s",i+1, menu[i].label,
	       menu[i].type == MENTRY_FUNC ? "\n" : ">>\n");

    if(!oneshot)
    {
	if(last)
	    printf("%d)  %s\n", quit, last);
	else
	    printf("%d)  Quit\n", quit);
    }
}

static int
get_selection(const char *prompt)
{
    int		num, c;

    fputs(prompt, stdout);
    fflush(stdout);
    num = 0;

    /*
    ** Access the console input in "raw" mode so we can allow the user
    ** to exit the menu by pressing the ESC key.
    */
    while((c = SerGetByte()) != '\r')
    {
	if(isdigit(c))
	    num = num*10 + (c - '0');
	else if(c == BACKSPACE)
	    num /= 10;
	else if(c == CANCEL)
	    return 0;
	else if(c == ESC)
	    return -1;
	/* Echo the character back */
	SerPutByte(c);
    }
    SerPutByte(c);
    SerPutByte('\n');
    
    return num;
}

/**
 * Display a menu on the console.
 * This function displays a menu on the console and waits for user
 * input.  The menu callbacks are executed as the user selects them.
 * The function returns when the user selects "quit" (the last menu
 * entry).
 * 
 * @param  menu  pointer to Menu data structure
 * @param  last  title for "quit" menu entry.
 * @return 0 if successful, -1 on error.
 */
int 
show_menu(Menu *menu, const char *last)
{
    int			quit, sel;
    char		prompt[40];

    quit = menu->nr_entries;
    if(menu->type == MENU_PERM)
	quit++;
    
    if(menu->nr_entries > MAX_MENU_LINES)
	return -1;

    sprintf(prompt, "\nEnter selection (1-%d) => ", quit);

    do
    {
	display_menu(menu->title, menu->entries, menu->nr_entries, 
		     last, menu->type == MENU_POPUP);

	/* Get the user's selection */
	sel = get_selection(prompt);
	if(sel < 0 || (menu->type == MENU_PERM && sel == quit))
	    break;
	
	if(sel > 0 && sel <= menu->nr_entries)
	{
	    MenuEntry *me = &menu->entries[sel-1];
	    MenuCallback f;
	    
	    if(me->type == MENTRY_FUNC)
	    {	
		/* Execute menu entry handler */
		f = (MenuCallback)me->action;
		(f)(me->call_data);
	    }
	    else	/* Recursively display submenu */
		show_menu((Menu*)me->action, menu->title);
	}
  	
    } while(menu->type != MENU_POPUP);

    return 0;
}
