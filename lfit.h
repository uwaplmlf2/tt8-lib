/*
** $Id: lfit.h,v 4a9de31628cd 2007/04/20 21:53:14 mikek $
*/
#ifndef _LFIT_H_
#define _LFIT_H_

double lfit(double x[], double y[], int n, double c[]);

#endif /* _LFIT_H_ */
