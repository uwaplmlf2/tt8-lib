/*
** $Id: timer.c,v 4a9de31628cd 2007/04/20 21:53:14 mikek $
**
** Interval timer module.  The functions in this module can manage up to
** MAX_ENTRIES simultaneous interval timers using the PIC alarm.
**
*/
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <tt8lib.h>
#include <tt8.h>
#include <tpu332.h>
#include <tat332.h>
#include <sim332.h>
#include <qsm332.h>
#include <dio332.h>
#include <tt8pic.h>
#include "eidi.h"
#include "q.h"
#include "timer.h"

#define MAX_ENTRIES	32

static void pic_handler(void);
static void c_handler(short sr);

typedef struct {
    long	counter;
    tfunc	callback;
} thandler_t;

static thandler_t 	handlers[MAX_ENTRIES];
static Q_t 		*timerq = 0L;
static int 		last_handler = 0;
static unsigned long 	old_pic_handler = 0;
static unsigned short 	save_syncr;

#define CmpAlarmMask	0x82
#define PREnables	0x23

static void
pic_alarm_enable(long alarm_time)
{
    SetAlarmSecs(alarm_time);
    PConfInp(F, 3);
    PConfBus(F, 3);
    PicOrRF(PREnables, CmpAlarmMask);
}

/*
** Assembler part of the PIC ISR.  Passes the saved status register value
** to the C part of the handler.  Also takes care of saving and restoring
** the register set.
*/
#ifdef __GNUC__
asm(".text
     .align 4
pic_handler:
    movem.l   %a0-%a6/%d0-%d7,-(%sp)
    move.w    60(%sp),-(%sp)
    jsr       c_handler
    addq.l    #2,%sp
    movem.l   (%sp)+,%a0-%a6/%d0-%d7
    rte
");
#else /* AZTEC C */
#asm
    cseg
_pic_handler:
    movem.l    a0-a6/d0-d7,-(sp)
    move.w     60(sp),-(sp)
    jsr        _c_handler
    addq.l     #2,sp
    movem.l    (sp)+,a0-a6/d0-d7
    rte
#endasm
#endif


static void
c_handler(short sr)
{
    int			index, r;
    short		old_sr;
    unsigned short	slow_clock = 0;
    long		t;

    if((*SYNCR & 0xff00) == 0)
    {
	slow_clock = *SYNCR;
	*SYNCR = save_syncr;
    }
    
    
    /*
    ** Acknowledge interrupt from PIC
    */
    PConfInp(F, 3);
    PicAckCmpAlrm();
    PicAndRF(PREnables, ~CmpAlarmMask);
    
    if(Pin(F, 3) == 0)
    {
	SetAlarmSecs(0L);
	PicAckCmpAlrm();
    }

    old_sr = GetStatusReg();
    
    /*
    ** Execute the handler for each expired timer and if the handler returns
    ** non-zero, re-insert it into the queue.  
    */
    t = RtcToCtm();
    while(Qnonempty(timerq) && Qfirstkey(timerq) <= t)
    {
	index = getfirst(timerq);
	if(handlers[index].callback)
	{
	    SetStatusReg(sr);
	    r = (*handlers[index].callback)();
	    SetStatusReg(old_sr);
	    if(r)
		insertd(timerq, index, handlers[index].counter + t);
	}

	t = RtcToCtm();
    }

    /* Set the next alarm */
    if(Qnonempty(timerq))
	pic_alarm_enable(Qfirstkey(timerq));

    if(slow_clock)
	*SYNCR = slow_clock;
}


/**
 * init_timer - initialize timer data structures.
 *
 * Initialize the interval timer data structures.  Returns 1 if
 * successful, otherwise 0.
 */
int
init_timer(void)
{
    unsigned long	*vbr;

    if((timerq = newqueue(MAX_ENTRIES)) == NULL)
	return 0;
    memset(handlers, 0, sizeof(handlers));

    vbr = (unsigned long*)GetVBR();
    if(vbr[Level_3_Interrupt] != (unsigned long)pic_handler)
    {
	old_pic_handler = vbr[Level_3_Interrupt];
	vbr[Level_3_Interrupt] = (unsigned long)pic_handler;
    }

    /*
    ** This hack is necessary if we use a timer to exit from a low
    ** power stop state.  The StopLP clears the "Y" bits from the
    ** clock synthesizer control register to set the clock to its
    ** lowest possible frequency.  Unfortunately, the ISR needs to
    ** call RtcToCtm which will not work properly with the slowed
    ** clock so we store the state of SYNCR here and restore it
    ** in the ISR.
    */
    save_syncr = *SYNCR;
    return 1;
}

/**
 * shutdown_timer - disable interval timer system
 *
 * Disable all active timers and free the data structures.
 */
void
shutdown_timer(void)
{
    unsigned long	*vbr;

    if(timerq == 0)
	return;
    unset_all_timers();

    if(old_pic_handler)
    {
	vbr = (unsigned long*)GetVBR();
	vbr[Level_3_Interrupt] = old_pic_handler;
    }

    delete_queue(timerq);
    timerq = 0;
}

/**
 * set_timer - call function when timer expires.
 * @secs: timer expiration in seconds
 * @callback: function to be called when timer expires.
 *
 * Set a timer and install a handler which will be called when the
 * timer expires.  If @callback returns non-zero, the timer will
 * be reloaded (i.e. @callback will be executed periodically).
 * Returns a positive non-zero timer ID or 0 if an error occurs.
 */
int
set_timer(long secs, tfunc callback)
{
    int			i;
    short		state;
    long		t;
    
    if(timerq == (Q_t*)0 && !init_timer())
	return 0;
    
    if(secs <= 0L || timerq->nr_entries >= MAX_ENTRIES)
	return 0;

    t = RtcToCtm();
    
    /* Find a free location in the handler array */
    for(i = 0;i < MAX_ENTRIES;i++)
	if(handlers[i].callback == 0)
	    break;
    if(i == MAX_ENTRIES)
	return 0;

    /* Set the new array entry */
    handlers[i].callback = callback;
    handlers[i].counter = secs;

    /*
    ** Add the entry to the delta queue.  The key is set to the timer
    ** expiration time.
    */
    cli(state);
    PicAckCmpAlrm();
    SetAlarmSecs(0L);
    PicAckCmpAlrm();
    insertd(timerq, i, secs+t);
    pic_alarm_enable(Qfirstkey(timerq));
    sti(state);

#ifdef TIMER_DEBUG
    fputs("set_timer: ", stderr);
    dump_queue(timerq, stderr);
#endif    
    return i+1;
}

static long suspend_time;

/**
 * suspend_timers - suspend timer operation.
 *
 * Suspend operation of the interval timer system.  All timers remain
 * queued.  The timers can be restarted using resume_timers().
 */
void
suspend_timers(void)
{
    short	state;
    
    if(timerq)
    {
	cli(state);
	PicAckCmpAlrm();
	SetAlarmSecs(0L);
	PicAckCmpAlrm();
	suspend_time = RtcToCtm();
	sti(state);
    }
}

/**
 * resume_timers - resume timer operation.
 *
 * Resume the suspended timers.  Timers start-up at the point they left
 * off, no attempt is made to catch up.
 */
void
resume_timers(void)
{
    if(timerq && suspend_time > 0)
    {
	if(Qnonempty(timerq))
	{
	    Qfirstkey(timerq) += (RtcToCtm() - suspend_time);
	    pic_alarm_enable(Qfirstkey(timerq));
	}
    }
}

/**
 * unset_timer - unset a specifed timer.
 * @id: timer ID returned from set_timer()
 *
 * Unset a timer.
 */
void
unset_timer(int id)
{
    short	state;
    int		item = id-1;

    cli(state);

    if(handlers[item].callback)
    {
	/*
	** If the timer is at the head of the queue, we must remove it and
	** reset the alarm time to the new first item.  Otherwise, we simply
	** remove it from the queue.
	*/
	if(item == Qfirstid(timerq))
	{
	    PicAckCmpAlrm();
	    SetAlarmSecs(0L);
	    PicAckCmpAlrm();
	    dequeue(timerq, item);
	    if(Qnonempty(timerq))
		pic_alarm_enable(Qfirstkey(timerq));
	}
	else
	    dequeue(timerq, item);
	
	handlers[item].counter = 0;
	handlers[item].callback = 0;
    }
    
    sti(state);
#ifdef TIMER_DEBUG
    fputs("unset_timer: ", stderr);
    dump_queue(timerq, stderr);
#endif

}

/**
 * unset_all_timers - unset every timer.
 *
 * Unset every timer.
 */
void
unset_all_timers(void)
{
    int		i;
    
    /*
    ** We can blindly pass every entry because unset_timer
    ** checks the validity.
    */
    for(i = 1; i <= MAX_ENTRIES;i++)
	unset_timer(i);
}
