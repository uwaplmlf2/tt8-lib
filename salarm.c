/*
** $Id: salarm.c,v 4a9de31628cd 2007/04/20 21:53:14 mikek $
**
** Simple alarm clock facility using the Periodic Interrupt Timer (PIT).  The
** PIT is programmed to generate an interrupt every 10 ms (one TICK).  These
** ticks form the basis of the alarm clock's timer.
**
*/
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <tt8lib.h>
#include <userio.h>
#include <tpu332.h>
#include <tt8.h>
#include <tat332.h>
#include <sim332.h>
#include <qsm332.h>
#include <dio332.h>
#include <tt8pic.h>
#include "eidi.h"
#include "salarm.h"


#define MSECS_PER_TICK	10L
#define	PITR_COUNT	0x64    /* number of ticks */
#define PITR_PRESCALE	0x00	/* 100usec per tick */
/* Sets timer period to 0.01s */
#define PITR_VALUE	(PITR_COUNT | PITR_PRESCALE)

static short alarm_type, alarm_isset = 0;
static long  counter_ticks;
static long  reload_ticks;
static void  (*alarm_action)(void);

static void pit_handler(void);

/*
 * pit_handler - PIT interrupt handler.  If an alarm is set, decrement
 * the counter.  When the counter reaches zero, call the user's handler
 * function.
 */
#ifdef __GNUC__
asm(".text
     .align 4
pit_handler:

    /* return if alarm not set */
    tst.w    alarm_isset
    bne      1f
    rte

    /* decrement counter, return unless zero */
1:  subq.l   #1,counter_ticks
    ble      2f
    rte

2:  tst.l    alarm_action
    beq      4f
    move.l   reload_ticks,counter_ticks
    tst.w    alarm_type
    beq      3f

    /* clear alarm flag and PIT then restore status reg. */
    move.w   #0,alarm_isset
    move.w   #0,0xfffffa24
3:  move.w   (%sp),%sr

    /* save regs, call user's function and restore regs */
    movem.l  %a0-%a6/%d0-%d7,-(%sp)
    move.l   alarm_action,%a0
    jsr      (%a0)
    movem.l  (%sp)+,%a0-%a6/%d0-%d7
4:  rte
");

#else /* AZTEC C */
#asm
        cseg
    _pit_handler:
        tst.w    _alarm_isset
	bne      .L1
	rte
.L1:
	subq.l   #1,_counter_ticks
	ble      .L2
	rte
.L2:
	tst.l    _alarm_action
	beq      .L4
	move.l   _reload_ticks,_counter_ticks
	tst.w    _alarm_type
	beq      .L3
	move.w   #0,_alarm_isset
	move.w   #0,$fffffa24
.L3:
	move.w   (sp),sr
	movem.l  a0-a6/d0-d7,-(sp)
        move.l   _alarm_action,a0
	jsr      (a0)
        movem.l  (sp)+,a0-a6/d0-d7
.L4:	rte
#endasm
#endif


#define ACTIVATE	(alarm_isset = 1)
#define DEACTIVATE	(alarm_isset = 0)


/**
 * alarm_set_ms - set an alarm.
 * @msecs: relative expiration time in milliseconds.
 * @type: alarm type.
 * @action: handler function.
 *
 * Start the alarm clock and install a handler function to be called
 * when the alarm expires.  The resolution of the alarm is 10ms.  @type
 * must be one of the following constants defined in salarm.h:
 *
 * %ALARM_ONESHOT - @action is called once after @msecs milliseconds
 *
 * %ALARM_REPEAT - @action is called every @msecs milliseconds
 *
 * Caveats:  the alarm interrupt is enabled while the user's handler is
 * executing.  If a repeating handler cannot execute within the alarm interval,
 * it must be re-entrancy safe.
 *
 * Returns 1 if successful, 0 if an alarm is already active.
 */
int
alarm_set_ms(long msecs, int type, void (*action)(void))
{
    unsigned long	*vbr;
    
    /*
    ** Return if alarm is active.
    */
    if(alarm_isset)
	return 0;

    /* Round to nearest tick */
    reload_ticks  = (msecs + (MSECS_PER_TICK >> 1))/MSECS_PER_TICK;
    counter_ticks = reload_ticks;
    alarm_type = type;
    alarm_action = action;

    vbr = (unsigned long*)GetVBR();
    vbr[PIT_INT_VECTOR] = (unsigned long)pit_handler;

    /* Loading the timer control register */
    *PITR = PITR_VALUE;
    ACTIVATE;
    
    return 1;
}

/**
 * alarm_reset - reset a pending alarm.
 *
 * Reset a pending alarm so that the countdown starts from the
 * current time.
 */
void
alarm_reset(void)
{
    short			state;
    
    if(!alarm_isset)
	return;
    
    cli(state);
    counter_ticks = reload_ticks;
    *PITR = PITR_VALUE;
    sti(state);

}

/**
 * alarm_shutdown - shutdown a pending alarm.
 *
 * Shutdown a pending alarm.
 */
void
alarm_shutdown(void)
{
    short	state;
    
    cli(state);
    DEACTIVATE;
    *PITR = 0;
    sti(state);
}
