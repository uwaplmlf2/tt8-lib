/**@file
** $Id: draw.h,v 4a9de31628cd 2007/04/20 21:53:14 mikek $
*/
#ifndef _DRAW_H_
#define _DRAW_H_

typedef enum { COLOR_BLACK=0,
	       COLOR_RED,
	       COLOR_GREEN,
	       COLOR_ORANGE,
	       COLOR_BLUE,
	       COLOR_YELLOW,
	       COLOR_CYAN,
	       COLOR_WHITE } drw_color;

void drw_clear(void);
void drw_moveto(int row, int col, int c);
void drw_setbg(drw_color color);
void drw_setfg(drw_color color);

#endif /* _DRAW_H_ */
