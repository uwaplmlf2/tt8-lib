/*
**$Id: crc.c,v 4a9de31628cd 2007/04/20 21:53:14 mikek $
**
** CRC generator.
**
*/
#define CCITT_POLY	0x8408

/*
** The CRC of a block of data + it's CRC is the following constant.
*/
int	crc_ok = 0x470f;

/**
 * crc - Generate a 16-bit CCITT CRC for a block of data.
 * @block: data buffer
 * @n: number of bytes to process
 *
 * Returns the 16-bit CRC of a block of data.  This is the CCITT CRC and
 * is based on the following polynomial.
 *
 * x^16 + x^12 + x^5 +1
 *
 */
unsigned short 
crc(char *block, int n)
{
    register int	i;
    unsigned short	c;
    unsigned		data;

    c = 0xffff;

    if(n <= 0)
	return 0;

    do
    {
	for(i = 0,data = (unsigned)0xff & *block++;i < 8;i++,data >>= 1)
	    if((c & 0x0001) ^ (data & 0x0001))
		c = (c >> 1) ^ CCITT_POLY;
	    else
		c >>= 1;
    } while(--n);

    c = ~c;				/* 1's complement */

    c = (c << 8) | ((c >> 8) & 0xff);	/* send CRC lsb first */

    return c;
}

