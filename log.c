/**@file
** $Id: log.c,v 04e89fb61b76 2008/04/29 17:33:15 mikek $
**
** Event/error logging facility.
**
*/
#include <stdarg.h>
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <stdlib.h>
#include <tt8lib.h>
#include <tpu332.h>
#include "log.h"

/** Maximum number of events between each logfile close/reopen.*/
#define SAVE_EVENTS	10

/**
** Maximum number of seconds between each logfile close/reopen.
*/
#define SAVE_TIME	60

static FILE *logfp = NULL;
static char logfname[16];
static long next_close;
static long linecount;

static int
do_open(const char *fname, const char *mode)
{
    if(fname == NULL)
	fname = logfname;
    
    if((logfp = fopen(fname, mode)) == NULL)
    {
	logfp = stderr;
	return 0;
    }

    next_close = RtcToCtm() + SAVE_TIME;
    linecount = SAVE_EVENTS;
    
    if(strcmp(logfname, fname))
	strncpy(logfname, fname, sizeof(logfname)-1);
    
    return 1;
}

static int
do_reopen(void)
{
    if(logfp == stderr)
	return 0;
    closelog();
    appendlog(logfname);
    return 1;
}

/*
 * Dump all errors to a separate file
 * Close the current log file, re-open it for reading and copy all of the
 * error messages to a separate file.
 *
 * @param  fname  name of error file.
 * @param  tail_seek  number of bytes from EOF to start error search.
 */
static int
dump_errors(const char *fname, long tail_seek)
{
    FILE	*ifp, *ofp;
    char	line[128];
    
    if(logfp == stderr)
	return 0;
    closelog();
    if((ifp = fopen(logfname, "r")) == NULL)
    {
	/* Try to reopen */
	appendlog(logfname);
	return 0;
    }
    
    /* Start 'tail_seek' bytes from the end of the file */
    if(tail_seek > 0)
	fseek(ifp, tail_seek, SEEK_END);
    
    if((ofp = fopen(fname, "w")) != NULL)
    {
	while(fgets(line, sizeof(line)-1, ifp) != NULL)
	{
	    if(!strncmp(&line[21], "ERROR", 5L))
		fputs(line, ofp);
	}
	
	fclose(ofp);
    }
    
    fclose(ifp);
    appendlog(logfname);
    return 1;
}


/**
 * Open a log file.
 * Open an event log file for use by log_error and log_event.
 *
 * @param  fname  file name
 * @return 1 if successful, otherwise 0.
 */
int
openlog(const char *fname)
{
    return do_open(fname, "w");
}

/**
 * Open an existing log file.
 * Like openlog() except the log file is appended to.
 *
 * @fname: file name
 * @return 1 if successful, otherwise 0.
 */
int
appendlog(const char *fname)
{
    if(do_open(fname, "a"))
    {
	return 1;
    }
    return 0;
}


/**
 * Close the current log file.
 */
void
closelog(void)
{
    if(logfp != NULL && logfp != stderr)
    {
	fclose(logfp);
	logfp = NULL;
    }
}

/**
 * Log an error.
 * Format and log an error message to the console and the log file (if open).
 * The message will be proceeded by the current date/time, the string "ERROR",
 * and by tag enclosed in parenthesis.  The @ag string can be used to group
 * error messages by "type" and comes in handy when searching the log file for
 * specific errors.
 *
 * @param  tag  a name to associate with this error type.
 * @param  str  printf style format string.
 */
void 
log_error(const char *tag, const char *str, ...)
{
    va_list	args;
    time_t	now = RtcToCtm();
    struct tm	*t;
    char	buf[34];

    va_start(args, str);
    t = localtime(&now);
    sprintf(buf, "%4d-%02d-%02d %02d:%02d:%02d: ERROR: ",
	    t->tm_year+1900, t->tm_mon+1, t->tm_mday,
	    t->tm_hour, t->tm_min, t->tm_sec);

    fputs(buf, stderr);
    fprintf(stderr, "(%s) ", tag);
    
    vfprintf(stderr, str, args);
    if(logfp != NULL && logfp != stderr)
    {
	fputs(buf, logfp);
	fprintf(logfp, "(%s) ", tag);
	vfprintf(logfp, str, args);
	fflush(logfp);
	if(--linecount == 0 || RtcToCtm() > next_close)
	    do_reopen();

    }
    
    va_end(args);
}

/**
 * Log an event.
 * Format and log a message to the console and the log file (if open).
 * The message will be proceeded by the current date/time.
 *
 * @param  str  printf style format string.
 */
void
log_event(const char *str, ...)
{
    va_list	args;
    time_t	now = RtcToCtm();
    struct tm	*t;
    char	buf[24];

    va_start(args, str);
    t = localtime(&now);
    sprintf(buf, "%4d-%02d-%02d %02d:%02d:%02d: ",
	    t->tm_year+1900, t->tm_mon+1, t->tm_mday,
	    t->tm_hour, t->tm_min, t->tm_sec);
    fputs(buf, stderr);
    vfprintf(stderr, str, args);
    
    if(logfp != NULL)
    {
	fputs(buf, logfp);
	vfprintf(logfp, str, args);
	fflush(logfp);
	if(--linecount == 0 || RtcToCtm() > next_close)
	    do_reopen();
    }

    
    va_end(args);
}

/**
 * Copy error messages to a separate file.
 * Copy all of the error messages from the current log to a
 * separate file. If tail_seek is greater than zero, start
 * searching for error messages this many bytes from the end
 * of the file.
 *
 * @param  file  file name for errors.
 * @param  tail_seek  starting point relative to end of file (if > 0).
 * @return 1 if successful, 0 if no log file exists.
 */
int
log_copy_errors(const char *file, long tail_seek)
{
    return dump_errors(file ? file : "errors.txt", tail_seek);
}
