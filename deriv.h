/*
** $Id: deriv.h,v 4a9de31628cd 2007/04/20 21:53:14 mikek $
*/
#ifndef _DERIV_H_
#define _DERIV_H_

typedef struct zdiff {
    int		n;
    int		size;
    double	h;
    double	*C;
    double	*ybuf;
    double	(*update)(struct zdiff*, double);
} zDiff;

#define zdiff_update(zd, y) ((*(zd)->update)(zd, y))

zDiff* zdiff_new(int nr_coeff, double coeff[], double h);
void zdiff_reset(zDiff *zdp);

#endif /* _DERIV_H_ */
