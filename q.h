/*
** $Id: q.h,v 4a9de31628cd 2007/04/20 21:53:14 mikek $
*/
#ifndef _Q_H_
#define _Q_H_


typedef struct {
    long	qkey;
    int		qnext;
    int		qprev;
} qent_t;

typedef struct {
    int		size;
    int		nr_entries;
    int		is_delta;
    qent_t	q[1];
} Q_t;

typedef void (*qhandler_t)(qent_t *e);

#define Qisempty(qp)	((qp)->nr_entries == 0)
#define Qnonempty(qp)	((qp)->nr_entries > 0)
#define Qfirstkey(qp)	((qp)->q[(qp)->q[(qp)->size].qnext].qkey)
#define Qlastkey(qp)	((qp)->q[(qp)->q[(qp)->size+1].qprev].qkey)
#define Qfirstid(qp)	((qp)->q[(qp)->size].qnext)

#define EMPTY	-1

int enqueue(Q_t *qp, int item);
int dequeue(Q_t *qp, int item);
int insert(Q_t *q, int item, long key);
int getfirst(Q_t *qp);
int getlast(Q_t *qp);
Q_t* newqueue(int size);
void delete_queue(Q_t *qp);
void clear_queue(Q_t *qp);
int insertd(Q_t *qp, int item, long key);
void dump_queue(Q_t *qp, FILE *ofp);

#endif
