/*
** $Id: util.c,v 6e2d920e8b8e 2007/05/24 21:58:01 mikek $
**
** Various utility functions.
**
*/
#include <stdio.h>
#include <stdlib.h>
#ifdef __GNUC__
#include <unistd.h>
#endif
#include <fcntl.h>
#include <ctype.h>
#include <tt8.h>
#include <tt8lib.h>

#define TOP_OF_STACK	0x204000
#define JSR_BASE	0x4e80
#define EXP_DUMP_ADDR	0x200000

typedef struct {
    unsigned long	pc;
    unsigned short	pad0;
    unsigned short	sr;
    unsigned long	usp;
    unsigned long	ssp;
    unsigned long	vbr;
    unsigned long	sfr;
    unsigned long	dfr;
    unsigned long	pad1;
    unsigned long	d[8];
    unsigned long	a[8];
    unsigned short	sr2;
    unsigned long	pc2;
    unsigned short	code;
} eframe_t;

static void __backtrace(unsigned long pc, unsigned long fp);

#ifdef __GNUC__
/**
 * backtrace - print a stack backtrace.
 *
 * Print a full stack backtrace to the console.
 */
void backtrace(void);

asm("    .text
         .align 4
         .global backtrace
backtrace:
        move.l    %a6,-(%sp)
        move.l    4(%sp),-(%sp)
        jsr       __backtrace
        addq.l    #8,%sp
        rts
");
#else
#asm
    cseg
    public _backtrace
_backtrace:
    move.l    a6,-(sp)
    move.l    4(sp),-(sp)
    jsr       ___backtrace
    addq.l    #8,sp
    rts
#endasm
#endif

static void
print_instr(int i, unsigned long fp, unsigned long raddr)
{
    unsigned short	*iptr;
    
    /*
    ** Look for the jsr instruction preceding the return address
    */
    iptr = (unsigned short*)raddr;
    if((iptr[-3] & 0xff80) == JSR_BASE)
	fprintf(stderr, "frame%d@%lx <%08lx>  jsr %04x%04x\n", i, fp, raddr-6,
		iptr[-2], iptr[-1]);
    else if((iptr[-2] & 0xff80) == JSR_BASE)
    {
	short offset = iptr[-1];
	fprintf(stderr, "frame%d@%lx <%08lx>  jsr %08lx\n", i, fp, raddr-4,
		raddr+offset-2);
    }
    else if((iptr[-1] & 0xff80) == JSR_BASE)
	fprintf(stderr, "frame%d@%lx <%08lx>  jsr ???\n", i, fp, raddr-2);
    else
	fprintf(stderr, "frame%d@%lx <%08lx>  ???\n", i, fp, raddr);
}

static void
__backtrace(unsigned long pc, unsigned long fp)
{
    int			i;
    unsigned long	last_fp, *sp;

    i = 0;
    print_instr(i, fp, pc);
    i++;
    
    last_fp = fp - 4;
    while(fp > last_fp && fp < TOP_OF_STACK)
    {
	/*
	 * Follow the stack frames assuming the following
	 * layout.  This is the standard for C functions
	 * unless the compiler is told to eliminate the
	 * frame pointer.
	 *
	 *  previous FP    (%sp)
	 *  return address (%sp+4)
	 */
	sp = (unsigned long*)fp;

	print_instr(i, fp, sp[1]);

	i++;
	last_fp = fp;
	fp = sp[0];
    }
}

/**
 * default_handler - default exception handler.
 *
 * Default exception handler.  Can be installed with InstallDefaultHandler
 * to provide more informative output for crashes.  It writes the current
 * register state along with a full stack backtrace to the console.
 */
void
default_handler(void)
{
    eframe_t		*eframe;
    int			i;
    
    eframe = (eframe_t*)EXP_DUMP_ADDR;
    printf("Exception %u at 0x%08lx\n", eframe->code & 0xfff, eframe->pc);

    /* Data registers */
    for(i = 0;i < 4;i++)
	printf("d%d=0x%08lx ", i, eframe->d[i]);
    fputc('\n', stdout);
    for(;i < 8;i++)
	printf("d%d=0x%08lx ", i, eframe->d[i]);
    fputc('\n', stdout);
   
    /* Address registers */
    for(i = 0;i < 4;i++)
	printf("a%d=0x%08lx ", i, eframe->a[i]);
    fputc('\n', stdout);
    for(;i < 8;i++)
	printf("a%d=0x%08lx ", i, eframe->a[i]);
    fputc('\n', stdout);

    __backtrace(eframe->pc, eframe->a[6]);

    LMDelay(20000);
    ResetToMon();
}

/**
 * yes_or_no - present user with a yes/no question on the console.
 * @prompt: question prompt
 * @def: specify default response
 *
 * This function prints @prompt to the console and waits for a response
 * from the user, 'y' or 'n'.  @def specifies the default response
 * if neither 'y' or 'n' are entered, non-zero @def means the default
 * response is 'y'.  The function returns 1 for 'y' and 0 for 'n'.
 */
int
yes_or_no(const char *prompt, int def)
{
    int		c, response;

    while(SerByteAvail())
	(void)SerGetByte();
    
    printf("\n%s (y or n)? [%c] ", prompt, def ? 'y' : 'n');
    fflush(stdout);
    
    response = c = tolower(getchar());

    if(c != '\n' && c != '\r')
	while((c = getchar()) != '\n' && c != '\r')
	    ;
    
    switch(response)
    {
	case 'y':
	    return 1;
	case 'n':
	    return 0;
    }

    return def;
}

/**
 * fileexists - check if named file exists
 * @name: filename
 *
 * Return true if the file @name exists.
 */
int 
fileexists(const char *name)
{
    int		fd;
    
    if((fd = open(name, O_RDONLY|O_BINARY)) < 0)
	return 0;
    close(fd);
    return 1;
}
