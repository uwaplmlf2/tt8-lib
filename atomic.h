/*
** $Id: atomic.h,v 4a9de31628cd 2007/04/20 21:53:14 mikek $
*/
#ifndef _ATOMIC_H_
#define _ATOMIC_H_

#ifdef __GNUC__
/**
 * atomic_set - atomically test and set a flag
 * @x: pointer to flag byte
 *
 * Atomically test and set a byte.  Returns true if the operation was
 * successful (i.e. the byte was not already set) or false if the
 * operation failed.
 */
static __inline__ int atomic_set(char *x)
{
    char c;
    
    __asm__ __volatile__("tas.b %1; seq %0" : "=d" (c), "=m" (*x) : "1" (*x));
    return c != 0;
}

/**
 * atomic_clear - atomically clear a flag
 * @x: pointer to flag byte
 *
 * Atomically clear a byte.
 */
static __inline__ void atomic_clear(char *x)
{
    __asm__ __volatile__("clr.b %0" : "=m" (*x));
}
#else /* AZTEC C */
#asm
    cseg
_atomic_set:
    movem.l    a0,-(sp)
    move.l     8(sp),a0
    clr.l      d0
    tas.b      (a0)
    seq.b      d0
    movem.l    (sp)+,a0
    rts

_atomic_clear:
    movem.l    a0,-(sp)
    move.l     8(sp),a0
    clr.b      (a0)
    movem.l    (sp)+,a0
    rts
#endasm
#endif

#endif /* _ATOMIC_H_ */
