/*
** $Id: qdec.h,v 4a9de31628cd 2007/04/20 21:53:14 mikek $
*/
#ifndef _QDEC_H_
#define _QDEC_H_

int qdec_stop(short chan);
int qdec_reset(short chan);
long qdec_read(short chan);
int qdec_start(short chan, short pri, short sec);

#endif
