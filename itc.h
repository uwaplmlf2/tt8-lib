/*
** $Id: itc.h,v 4a9de31628cd 2007/04/20 21:53:14 mikek $
*/
#ifndef _ITC_H_
#define _ITC_H_

int itc_stop(short chan);
int itc_reset(short chan);
unsigned long itc_read(short chan);
int itc_start(short chan);

#endif
