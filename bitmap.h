/*
** $Id: bitmap.h,v 4a9de31628cd 2007/04/20 21:53:14 mikek $
*/
#ifndef _BITMAP_H_
#define _BITMAP_H_

#define __whichbyte(b, n)	(b[(n) >> 3])
#define __selbit(n)		(1 << ((n) & 0x07))

/**
 * BSET - set a bit in a bitmap
 * @b: bitmap (array of unsigned chars)
 * @n: bit number
 *
 * Set bit @n in the bitmap.  No bounds checking is done.
 */
#define BSET(b, n)		(__whichbyte(b,n) |= __selbit(n))

/**
 * BCLR - clear a bit in a bitmap
 * @b: bitmap (array of unsigned chars)
 * @n: bit number
 *
 * Clear bit @n in the bitmap.  No bounds checking is done.
 */
#define BCLR(b, n)		(__whichbyte(b,n) &= ~(__selbit(n)))

/**
 * BTST - test a bit in a bitmap
 * @b: bitmap (array of unsigned chars)
 * @n: bit number
 *
 * Return the state of bit @n in the bitmap.  No bounds checking is done.
 */
#define BTST(b, n)		(__whichbyte(b,n) & __selbit(n))

#endif /* _BITMAP_H_ */
