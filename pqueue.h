/**@file
** $Id: pqueue.h,v 4a9de31628cd 2007/04/20 21:53:14 mikek $
*/
#ifndef _PQUEUE_H_
#define _PQUEUE_H_

/** comparison function type */
typedef int (*fcmp)(void*, void*);

/** Priority queue */
typedef struct {
    long		size;	/**< Maximum queue size */
    long		n;	/**< Number of elements */
    fcmp		cmp;	/**< Comparison function */
    void*		e[1];	/**< Elements */
} PQueue;

PQueue *pq_create(long size, fcmp f);
void pq_destroy(PQueue *pq);
int pq_insert(PQueue *pq, void *e);
void* pq_remove(PQueue *pq);
void* pq_peek(PQueue *pq);

#ifdef __GNUC__
/**
 * Check whether priority queue is empty.
 *
 * @param  pq  pointer to PQueue.
 * @return true if queue is empty.
 */
static __inline__ int pq_empty(PQueue *pq)
{
    return (pq->n == 0);
}
#else
int pq_empty(PQueue *pq);
#endif

#endif /* _PQUEUE_H_ */
