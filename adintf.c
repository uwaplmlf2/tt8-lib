/**@file
** $Id: adintf.c,v 4a9de31628cd 2007/04/20 21:53:14 mikek $
**
** Module to allow the reading of multiple A/D channels "simultaneously".
** The channels are read with the minimum delay the QSPI interface will
** allow, 10 usecs for a 16mhz clock.
**
*/
#include <tt8.h>
#include <tat332.h>
#include <sim332.h>
#include <qsm332.h>
#include <dio332.h>
#include <tt8lib.h>
#include "adintf.h"

#define MAX186_BAUD	4


/* A/D Channel selection codes */
static const int ADchan_codes[8] = {
    0x8f, 0xcf, 0x9f, 0xdf, 0xaf, 0xef, 0xbf, 0xff };


/**
 * Initialize the A/D channels for "burst" mode.
 * This function initializes the QSPI interface to the MAX186 A/D to read all
 * specified channels in a single burst.  Note that the inter-sample timings
 * and the QSPI baud rate assume the TT8 clock is running at 16mhz.  The clock
 * should be set to this frequency before sampling the A/D.
 *
 * @param  chans  an array of A/D channel numbers.
 * @param  n   number of channels (max 8).
 *
 */
void
set_ad_params(int chans[], int n)
{
    int			i, dsckl, dtl;
    unsigned		mask;

    if(n > 8)
	n = 8;

    /*
    ** Delay parameters.  DSCKL specifies the delay from chip-select
    ** valid to the first clock transition for the serial transfer.
    ** DTL specifies the delay after each serial transfer.  We use
    ** a 2Mhz baud rate and a transfer size of 16 bits so each transfer
    ** requires 8 usecs.  Refer to Chapter 5 of the MC68332 User's
    ** Manual.
    **
    ** The values hardcoded here will result in the minimum delay (10 usecs)
    ** between channel sampling.
    */    
    dsckl = 15;
    dtl = 0;

    /* QSPI needs MOSI, MISO, and chip-select */
    *QPAR = (M_MOSI | M_MISO | M_PCS3);
    
    /* 
    ** Output values of GP-I/O pins AND the state of the chip-select lines
    ** between QSPI transfers. The MAX186 CS uses inverted logic so it must
    ** be held high to disable. All other CS lines are held low.
    */
    //*QPDR &= ~(M_PCS0 | M_PCS1 | M_PCS2);
    //*QPDR = (M_PCS3 | M_SCK | M_MISO);
    *QPDR = M_PCS3;
    
    /* All chip-selects, MOSI and SCK are outputs */
    *QDDR = M_PCS3 | M_PCS2 | M_PCS1 | M_PCS0 | M_SCK | M_MOSI;

    /*
    ** Set QSPI control registers
    */

    /* Specify baud rate and set Master Mode */
    *SPCR0 = MAX186_BAUD | (M_MSTR & SET);

    /* Command queue parameters */
    *SPCR2 = 0			/* Starting queue pointer */
      | (n << 8)		/* Ending queue pointer */
      | (M_WREN & CLR);		/* Disable queue wrap-around */

    /* Disable Loop Mode, HALTA interrupts, and Halt */
    *SPCR3 = 0;

    /* Set delays */
    *SPCR1 = ((dsckl & 0x7f) << 8) | (dtl & 0xff);
    
    /*
    ** Load the transmit registers with the A/D channel
    ** select codes.  The last register contains a zero
    ** as it will only be used to clock-in the A/D channel
    ** selected by SPIXMT[n-1].
    */
    for(i = 0;i < n;i++)
	SPIXMT[i] = ADchan_codes[chans[i]];
    SPIXMT[n] = 0;


    /* chip select mask */
    mask = (M_CS0 & CLR) | (M_CS1 & CLR) | (M_CS2 & CLR) | (M_CS3 & CLR);

    /*
    ** Load command registers.  The first command only clocks out the lower
    ** 8 bits of the transmit register while subsequent commands must clock
    ** out the full 16-bits as they will also be clocking-in the A/D value.
    */
    SPICMD[0] = (M_CONT & SET)
      | (M_DT & (dtl ? SET : CLR))
      | (M_DSCK & SET) | mask;
    SPICMD[1] = (M_CONT & SET)
      | (M_BITSE & SET)
      | (M_DT & (dtl ? SET : CLR))
      | (M_DSCK & SET) | mask;
    for(i = 2;i <= n;i++)
	SPICMD[i] = SPICMD[1];

    /* Clear the CONT bit in the last command */
    SPICMD[n] &= ~M_CONT;
    
    /* Clear interrupt flag */
    *SPSR &= ~M_SPIF;
}

#ifndef __GNUC__
/**
 * Read multiple channels of data from the A/D.
 * This function reads a single value from @n channels of the internal
 * A/D.  The A/D must be configured with set_ad_params() before calling
 * this function.
 *
 * @param  buf  buffer for storing the A/D values.
 * @param  n  number of channels.
 */
void
read_ad_chans(unsigned short *buf, int n)
{
    volatile unsigned short	*datap;
    
    /* Start the QSPI */
    *SPCR1 |= M_SPE;

    /* Wait for operation to complete */
    while(!(*SPSR & M_SPIF))
	;

    /* Stop the QSPI */
    *SPCR1 &= ~M_SPE;
    *SPSR &= ~M_SPIF;
    
    /*
    ** Read data from the QSPI receive RAM.  Note that the data starts
    ** at the second word.  The 12-bit A/D value is shifted left 3 bits
    ** so we shift it right before copying.
    */
    datap = &SPIRCV[1];
    while(n-- > 0)
	*buf++ = (*datap++ >> 3);
}

unsigned short
read_ad(int chan)
{
    int			i, chans[8];
    unsigned short	x[8];
    
    for(i = 0;i < 8;i++)
	chans[i] = chan;
    set_ad_params(chans, 8);
    read_ad_chans(x, 8);
    return x[7];
}

#endif /* !__GNUC__ */
