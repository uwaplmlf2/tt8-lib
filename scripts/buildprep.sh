#!/usr/bin/env bash
#
# Prepare the build directory and run the configuration phase
#

set -e
type cmake >&2
cmake -S . -B build -DCMAKE_TOOLCHAIN_FILE=cmake/mlf2gcc.cmake

echo "Run 'cmake --build build' to build firmware"
