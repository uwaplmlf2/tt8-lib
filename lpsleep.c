/**@file
**$Id: lpsleep.c,v 8ddaa06450b4 2007/07/11 02:46:22 mikek $
**
** Low-power sleep interface functions.  If this file is compiled with
** the preprocessor macro NO_3v defined, the processor will remain at
** 5v during sleep.
**
*/
#undef DEBUG
#include <tpu332.h>
#include <tt8.h>
#include <tat332.h>
#include <sim332.h>
#include <qsm332.h>
#include <dio332.h>
#include <tt8pic.h>
#include <picodcf8.h>
#include <stdio.h>
#include <string.h>
#include <tt8lib.h>
#include <time.h>
#include <stdlib.h>
#include <userio.h>
#include "log.h"
#include "hash.h"
#include "lpsleep.h"

#ifndef V2CardPower
enum {
    _V2CardPower = 32000,       // turn on (1) or off (0) card and buffers
    _V2CardReset                        // reset the flash card
};
#define V2CardPower(onoff)              initcf(_V2CardPower, onoff)
#define V2CardReset(onoff)              initcf(_V2CardReset, 0)
#endif

#ifdef NO_3v
#define VSWITCH(v)
#else
#define VSWITCH(v)	VRegSwitchTo((v), (ushort)64000, NO_RUPTS_MASK)
#endif

static void ad_flush(void);
static int lpmode(time_tt wakeup, short allow_serial);


static int (*lpfp)(time_tt, short);

#define OP_RTS	0x4e75

/**
 * @deprecated
 * Copy lpmode() into RAM.
 * Copy the lpmode function into RAM where it will be executed through
 * a function pointer.  This function should be called at startup if
 * the following conditions are true.
 * - The program is loaded in FLASH
 * - The processor is switched to 3v during sleep
 *
 * The purpose of this exercise is to work around a possible bug in
 * the FLASH which can result in Illegal Instruction exceptions when
 * operating at 3v.
 */
int
copy_lp_function()
{
    register unsigned short	*src, *dst, *p;
    register short		fsize;

    lpfp = 0;
#ifndef NO_3v
    /*
    ** Find the end of the function using the same hack as Onset's 'ramrun' 
    ** example, look for the RTS opcode.
    */
    p = src = (unsigned short*)lpmode;
    while(*p++ != OP_RTS)
	;
    fsize = p - src;

    /* Allocate a block of RAM for the function */
    if((dst = (unsigned short*)malloc(fsize * sizeof(short))) == NULL)
	return 0;

#ifdef USE_LOG
    log_event("Copying lpmode from %p to %p (%d words)\n", src, dst, fsize);
#endif

    p = dst;
    while(fsize-- > 0)
	*p++ = *src++;
    lpfp = (int (*)(time_tt, short))dst;
#endif
    return 1;
}

/*
** Bitmask specifying TPU pins which should not be touched when
** entering low-power-sleep mode.
*/
static unsigned short TPU_DONT_TOUCH = 0;

/**
 * Protect a TPU pin from LP-mode fiddling.
 * Protect a TPU pin.  Before entering LP sleep mode, all TPU pins
 * are configured as inputs.  This function allows "protecting" a
 * pin so it will be left in its pre-sleep-mode state.
 *
 * @param  pnum  TPU pin number
 *
 */
void
protect_tpu_pin(unsigned short pnum)
{
    if(pnum < 16)
	TPU_DONT_TOUCH |= (1 << pnum);
}

/**
 * Allow TPU pin state to be changed.
 * Unprotect a TPU pin -- allow the pin to be configured as an input
 * before entering LP sleep mode.
 *
 * @param  pnum  TPU pin number
 */
void
unprotect_tpu_pin(unsigned short pnum)
{
    if(pnum < 16)
	TPU_DONT_TOUCH &= ~(1 << pnum);
}


static HashTable	*before_hooks, *after_hooks;

/**
 * Add a function to be called before sleeping.
 * Add a function (hook) to the list of functions which will be
 * called before the system is switched to LP-sleep mode.  Note that
 * name can be any string you choose (i.e. it doesn't have to be the
 * name of the function).
 *
 * The functions are executed in arbitrary order.
 *
 * @param  name  name to associate with function.
 * @param  h  function pointer
 * @return 1 if the function was successfully installed, otherwise 0.
 */
int
add_before_hook(const char *name, sleephook h)
{
    if(before_hooks == NULL && 
       (before_hooks = ht_create(17, HT_STANDARD)) == NULL)
	return 0;
    
    return ht_insert_elem(before_hooks, name, h, 0);
}

/**
 * Remove function from "before" hooks list.
 * Remove the named function from the list of "before" hooks.
 *
 * @param  name  name associated with function.
 */
void
remove_before_hook(const char *name)
{
    if(before_hooks)
	ht_remove_elem(before_hooks, name);
}


/**
 * Add a function to be called after sleeping.
 *
 * @param  name  name to associate with function.
 * @param  h  function pointer
 * @return 1 if the function was successfully installed, otherwise 0.
 * @sa add_before_hook()
 */
int
add_after_hook(const char *name, sleephook h)
{
    if(after_hooks == NULL && 
       (after_hooks = ht_create(17, HT_STANDARD)) == NULL)
	return 0;
    
    return ht_insert_elem(after_hooks, name, h, 0);
}

/**
 * Remove function from "after" hooks list.
 * Remove the named function from the list of "after" hooks.
 *
 * @param  name  name associated with function.
 */
void
remove_after_hook(const char *name)
{
    if(after_hooks)
	ht_remove_elem(after_hooks, name);
}

static void
exec_hook(struct elem *e, void *calldata)
{
    sleephook	h = (sleephook)e->data;

#ifdef DEBUG
    printf("Running function:  '%s' ...\n", e->name);
#endif    
    (*h)();
}


/**
 * Initialize function lists.
 * Initialize the function lists.  This must be called before any of
 * the add*hook() or remove*hook() functions.
 *
 */
void
init_sleep_hooks()
{
    TPU_DONT_TOUCH = 0;

    if(after_hooks == NULL)
	after_hooks = ht_create(17, HT_STANDARD);
    if(before_hooks == NULL)
	before_hooks = ht_create(17, HT_STANDARD);
    
    add_after_hook("A/D-flush", ad_flush);
}

/*
** Prepare all the I/O pins for low-power mode.  This function was
** taken almost directly from the Onset example code.
*/
static void 
lp_setup_pins(void)
{
    register int		pin;
    register unsigned short 	bit;
    ucpv			port, dir, assign;

    PConfInp(F,3);
    PConfInp(F,1);
    
    PConfOutp(F,7); PClear(F,7);
    PConfInp(F,6);
    PConfInp(F,5);
    PInpToOut(F,4); PClear(F,4);
    PInpToOut(F,2); PClear(F,2);
    PConfInp(F,0);

    /* PInpToOut(E, 0..7) */
    port = PORTE;
    dir = DDRE;
    assign = PEPAR;

    *assign = 0;
    *dir = 0;
    *port = 0;
    *dir = 0xff;

    PConfBus(D,7);
    PConfOutp(D,6); PSet(D,6);
    PInpToOut(D,5); PClear(D,5);
    PInpToOut(D,4); PClear(D,4);
    PInpToOut(D,3); PClear(D,3);
    PConfOutp(D,2); PClear(D,2);
    PConfOutp(D,1); PClear(D,1);
    PInpToOut(D,0); PClear(D,0);

    /*
    ** Make sure all "unprotected" TPU pins are set to be inputs
    */
    for(pin = 0,bit = 1;pin < 16;pin++,bit<<=1)
	if(!(bit & TPU_DONT_TOUCH))
	    (void)TPUGetPin(pin);
}

/*
** Interrupt handlers.
*/
#define NONE			0
#define SERIAL_INTERRUPT	1
#define PIC_INTERRUPT		2

volatile short which_interrupt;

static void 
serial_int_handler(void)
{
    PConfInp(F,1);
    which_interrupt = SERIAL_INTERRUPT;
}

static void 
pic_int_handler(void)
{
    PConfInp(F,3);
    which_interrupt = PIC_INTERRUPT;
}

static void
spurious_int_handler(void)
{
    return;
}


#define CmpAlarmMask	0x82
#define PREnables	0x23


/**
 * Go into low power sleep mode.
 * Put the Model 8 into low power mode until the specified time.  If the
 * wait time is less than 1 second, we do not switch to low power mode
 *
 * Returns 0 on normal return, 1 if a serial interrupt occurred, or
 * one of the following negative error codes (defined in lpsleep.h):
 * - LP_TOOSHORT (sleep interval too short, < 1 second)
 * - LP_IRQBUSY  (the IRQ3 line is already asserted)
 *
 * When the return value is LP_IRQBUSY, the function attempts to clear the
 * line before returning so it is safe to try again.
 *
 * @param  wakeup  wakeup time (seconds and ticks).
 * @param  allow_serial  if non-zero allow serial interrupt.
 * @return 0 on normal return, 1 on serial interrupt, < 0 on error.
 */
int 
lp_sleep_till(time_tt wakeup, short allow_serial)
{
    register long	diff;
    int			r = 0, intmask;
    vfptr		oldser, oldspur, oldpic;
    static ExcCFrame	irq1fp, irq3fp, spurfp;

#ifdef __GNUC__
    if((diff = ttmcmp(wakeup, ttm_now())) < GetTickRate())
	return LP_TOOSHORT;
#else
    if((diff = ttmcmp(wakeup, ttmnow())) < GetTickRate())
	return LP_TOOSHORT;
#endif

    /*
    ** If IRQ3 line is asserted, we have a problem.
    */
    PConfInp(F,3);
    if(Pin(F,3) == 0)
    {
#ifdef USE_LOG
	log_error("lpsleep", "IRQ3 already asserted -- trying to clear\n");
#else
	printf("LPsleep error:  IRQ3 already asserted! -- trying to clear\n");
#endif
	PicAckCmpAlrm();
	SetAlarmSecs(0L);
	PicAckCmpAlrm();
	return LP_IRQBUSY;
    }

    ht_foreach_elem(before_hooks, exec_hook, (void*)0);

    lp_setup_pins();

    if(allow_serial)
        oldser = InstallHandler(serial_int_handler, Level_1_Interrupt, 
				&irq1fp);

    /* Catch spurious interrupts */
    oldspur = InstallHandler(spurious_int_handler, Spurious_Interrupt, 
			     &spurfp);
    

    intmask = GetInterruptMask();
    SetInterruptMask(NO_RUPTS_MASK);
    
    /*
    ** Set up the PIC alarm interrupt
    */
    SetAlarmSecs(wakeup.secs);
    oldpic = InstallHandler(pic_int_handler, Level_3_Interrupt, &irq3fp);
    PConfInp(F, 3);
    PConfBus(F, 3);

    SerShutDown(TRUE, TRUE);
    
    /* Do it ... */
#ifndef NO_3v
    if(lpfp)
	r = (*lpfp)(wakeup, allow_serial);
    else
#endif
	r = lpmode(wakeup, allow_serial);
    
    /* Restore interrupt mask */
    SetInterruptMask(intmask);

    /* Restore previous handler */
    InstallHandler(oldspur, Spurious_Interrupt, 0);
    InstallHandler(oldpic, Level_3_Interrupt, 0);
    
    ht_foreach_elem(after_hooks, exec_hook, (void*)0);

    return r;
}

#define LP_CLOCK_FREQ	4000000L

static int
lpmode(time_tt wakeup, short allow_serial)
{
    time_tt 	now;
    long	clock;

    if(allow_serial)
    {
	PConfInp(F,1);
	PConfBus(F,1);
    }
      
    clock = SimGetFSys();
    if(clock > LP_CLOCK_FREQ)
	SimSetFSys(LP_CLOCK_FREQ);

    which_interrupt = NONE;
    
    VSWITCH(v3);
       
    /* Enable PIC alarm interrupts */
    PicOrRF(PREnables, CmpAlarmMask);

    StopLP(0x2000);	/* Go to LP mode until interrupted */
	    
    SetInterruptMask(NO_RUPTS_MASK);
    
    /* Acknowledge the interrupt */
    PConfInp(F, 3);
    PicAckCmpAlrm();
    PicAndRF(PREnables, ~CmpAlarmMask);

    if(which_interrupt != PIC_INTERRUPT)
    {
	/*
	** Another interrupt woke us -- cancel the pending
	** alarm.
	*/
	PicAckCmpAlrm();	/* in case we missed it */
	SetAlarmSecs(0L);
    }
    else
    {
	/*
	** Busy-wait for any remaining time.
	*/
#ifdef __GNUC__
	while(ttmcmp(wakeup, (now = ttm_now())) > 0L)
	    ;
#else
	while(ttmcmp(wakeup, (now = ttmnow())) > 0L)
	    ;
#endif
    }
    
    VSWITCH(v5);
    SimSetFSys(clock);

    /* Power-on the CF card */
    V2CardPower(1);
    
    return (which_interrupt == PIC_INTERRUPT) ? 0 : 1;
}

static void 
ad_flush()
{
    register int i;

    /*
    ** lp_setup_pins powers down the A/D controller.  The first values
    ** read will be garbage.  Read all channels and discard.
    */
    for(i = 0;i < 8;i++)
	(void)AtoDReadWord(i);

}


#ifndef PWORD
#define PWORD		"tt8"
#define PWORD_LEN	3
#endif

/**
 * Go into an interruptible low power sleep.
 * Call lp_sleep_till() to put the TT8 into an interruptible low-power sleep 
 * mode until a specified future time.  The sleep my be interrupted by
 * asserting a BREAK on the serial line.  To protect against line noise 
 * interrupting the sleep, the user will be prompted to type a short sequence 
 * of characters (default "tt8").  If this sequence is not typed within a few 
 * seconds, the sleep will continue.
 *
 * @param  wake  wakeup time.
 * @return 1 if sleep was interrupted, otherwise 0.
 * @sa lp_sleep_till()
 */
int 
isleep_till(time_tt wake)
{
    int			r, c;
    char		pword[PWORD_LEN+1];
    register int	i;

    while((r = lp_sleep_till(wake, 1)) == 1)
    {
	/*
	** We got a serial interrupt but it might have been line noise
	** so we must prompt for the password.
        */
        SerInFlush();
	printf("Type %s:", PWORD);
	fflush(stdout);

	/*
	** Allow 2.5s input time for each character
	*/
        i = 0;
        while(i < PWORD_LEN && (c = SerTimedGetByte(2500L)) != -1)
            pword[i++] = c;
        pword[i] = 0;
	printf("\n");
        if(!strcmp(pword, PWORD))
            break;
    }

    return r;
}

/**
 * Interruptible low-power sleep mode.
 * Put the TT8 into an interruptible low-power sleep mode for a specified 
 * length of time.  This is a convenience function which adds @secs to the
 * current time to obtain the wakeup time and then calls isleep_till().
 *
 * @param  secs  sleep duration
 * @return 1 if sleep was interrupted, otherwise 0.
 * @sa isleep_till()
 */
int 
isleep(long secs)
{
    time_tt	now, wake;

#ifdef __GNUC__
    now = ttm_now();
#else
    now = ttmnow();
#endif
    wake.secs = now.secs + secs;
    wake.ticks = 0;

    return isleep_till(wake);
}

